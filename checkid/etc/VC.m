//
//  VC.m
//  checkid
//
//  Created by Ismail Ege AKPINAR on 04/03/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import "VC.h"
#import "PreferencesVC.h"

@interface VC ()

@end

@implementation VC

#pragma mark - Default init methods

- (void) viewDidLoad    {
    [super viewDidLoad];
    
    // Common navigation bar properties
    [self.navigationItem setHidesBackButton:YES];
    
    // Icon is small so always use the high res one
    UIImage *img_gear = [UIImage imageNamed:@"icon-gear-white@2x"];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, img_gear.size.width * 0.75f, img_gear.size.height * 0.75f)];
    [btn setBackgroundImage:img_gear forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(navbarRightTouch:) forControlEvents:UIControlEventTouchUpInside];
    // Insert space for padding
    UIBarButtonItem *bar_item1 = [[UIBarButtonItem alloc] initWithCustomView:btn];
    UIBarButtonItem *bar_item2 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    [bar_item2 setWidth:10.0f];
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:bar_item2, bar_item1, nil];
}

- (void) viewWillAppear:(BOOL)animated  {
    [super viewWillAppear:animated];
    
    if(!self.is_loaded) {
        self.is_loaded = YES;
        
        [self configureFrames];
    }
}

#pragma mark - UI callbacks
- (IBAction)navbarRightTouch:(id)sender {
    if(!_vc_nav_preferences)    {
        PreferencesVC *vc_preferences = [[PreferencesVC alloc] initWithNibName:@"Preferences" bundle:[NSBundle mainBundle]];
        _vc_nav_preferences = [[UINavigationController alloc] initWithRootViewController:vc_preferences];
        [_vc_nav_preferences.navigationBar setBarStyle:UIBarStyleBlack];
        [_vc_nav_preferences.navigationBar setTranslucent:YES];
    }
    
    [self presentModalViewController:_vc_nav_preferences animated:YES];
}

#pragma mark - Orientation methods

// NOTE: Prior to iOS6, this method should be implemented to support additional interface orientations. plist file isn't enough
- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation  {
    return YES;
}

- (void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    [self configureFrames];
}

- (void) configureFrames  {
    warn(@"configureFrames - Should be implemented by child controller");
}


@end
