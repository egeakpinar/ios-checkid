//
//  VC.h
//  checkid
//  This is an abstract UIViewController extension, with general use properties and methods
//
//  Created by Ismail Ege AKPINAR on 04/03/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import <UIKit/UIKit.h>

// NOTE: These includes will make the app linking slower, you might want to remove them for production
#import "Debug.h"
#import "Helper.h"
#import "CCCommon.h"

@interface VC : UIViewController

#pragma mark - Internal properties
@property (nonatomic, assign) BOOL is_loaded;
@property (nonatomic, retain) UINavigationController *vc_nav_preferences;

#pragma mark - External methods
- (IBAction)navbarRightTouch:(id)sender;

@end
