//
//  Common.h
//  YAction
//
//  Created by Ismail Ege AKPINAR on 06/01/2013.
//  Copyright (c) 2013 Quiet Riots. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CardReader.h"

@interface CCCommon : NSObject

// YES if this is the first time this app is launched
+ (BOOL) is_first_load;

+ (unsigned char *) stringToByteArray:(NSString *)input outputLength:(int *)outputLength;
+ (NSData *) stringToData:(NSString *)input;
+ (NSString *) dataToString:(NSData *)data;

@end
