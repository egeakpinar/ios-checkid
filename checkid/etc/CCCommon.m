//
//  Common.m
//  YAction
//
//  Created by Ismail Ege AKPINAR on 06/01/2013.
//  Copyright (c) 2013 Quiet Riots. All rights reserved.
//

#import "CCCommon.h"
#import "Debug.h"
#import "Helper.h"
#import "CardReader.h"

@implementation CCCommon

+ (BOOL) is_first_load  {
    NSUserDefaults *user_defaults = [NSUserDefaults standardUserDefaults];
    if(user_defaults && [user_defaults objectForKey:@"is_loaded"])  {
        return NO;
    }
    else {
        [user_defaults setObject:[NSNumber numberWithBool:YES] forKey:@"is_loaded"];
        if(![user_defaults synchronize])    {
            warn(@"Could not save is_loaded to user defaults");
        }
    }
    return YES;
}


+ (unsigned char *) stringToByteArray:(NSString *)input outputLength:(int *)outputLength    {
    unsigned char *output = NULL;
    
    const char *buf = [input UTF8String];
    NSMutableData *data = [NSMutableData data];
    if (buf)    {
        uint32_t len = strlen(buf);
		
        char singleNumberString[3] = {'\0', '\0', '\0'};
        uint32_t singleNumber = 0;
        for(uint32_t i = 0 ; i < len; i+=2)
        {
            if ( ((i+1) < len) && isxdigit(buf[i]) && (isxdigit(buf[i+1])) )    {
                singleNumberString[0] = buf[i];
                singleNumberString[1] = buf[i + 1];
                sscanf(singleNumberString, "%x", &singleNumber);
                uint8_t tmp = (uint8_t)(singleNumber & 0x000000FF);
                [data appendBytes:(void *)(&tmp) length:1];
            }
            else    {
                break;
            }
        }
	}
    if(data && [data length] > 0)   {
        output = (unsigned char*) malloc(sizeof(unsigned char) * data.length);
        [data getBytes:output length:data.length];
    }
    else    {
        warn(@"Failed to get bytes from string ||%@||", input);
    }
    
    *outputLength = sizeof(unsigned char) * data.length;
    
    return output;
}

+ (NSData *) stringToData:(NSString *)input {
    unsigned char *output = NULL;
    
    const char *buf = [input UTF8String];
    NSMutableData *data = [NSMutableData data];
    if (buf)    {
        uint32_t len = strlen(buf);
		
        char singleNumberString[3] = {'\0', '\0', '\0'};
        uint32_t singleNumber = 0;
        for(uint32_t i = 0 ; i < len; i+=2)
        {
            if ( ((i+1) < len) && isxdigit(buf[i]) && (isxdigit(buf[i+1])) )    {
                singleNumberString[0] = buf[i];
                singleNumberString[1] = buf[i + 1];
                sscanf(singleNumberString, "%x", &singleNumber);
                uint8_t tmp = (uint8_t)(singleNumber & 0x000000FF);
                [data appendBytes:(void *)(&tmp) length:1];
            }
            else    {
                break;
            }
        }
	}
    if(data && [data length] > 0)   {
        return data;
    }
    else    {
        warn(@"Failed to get bytes from string ||%@||", input);
    }
    
    return nil;
}

+ (NSString *) dataToString:(NSData *)data  {
    const unsigned char *dbytes = [data bytes];
    NSMutableString *hexStr =
    [NSMutableString stringWithCapacity:[data length]*2];
    int i;
    for (i = 0; i < [data length]; i++) {
        [hexStr appendFormat:@"%02x", dbytes[i]];
    }
    return [NSString stringWithString: hexStr];
}


@end
