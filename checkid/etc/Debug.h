//
//  Debug.h
//  YAction
//
//  Created by Ismail Ege AKPINAR on 26/12/2012.
//  Copyright (c) 2013 i-DENTITY. All rights reserved.
//


#define DEBUG_MODE YES

#define DEBUG_LOGGING_ON
#define WARNING_LOGGING_ON
#define ERROR_LOGGING_ON

#ifdef DEBUG_LOGGING_ON
#define logg( s, ... ) NSLog( @"%@", [NSString stringWithFormat:(s), ##__VA_ARGS__] )
#define deb( s, ... ) NSLog( @"*** DEBUGGING *** %@", [NSString stringWithFormat:(s), ##__VA_ARGS__] )
#else
#define logg( s, ... )
#define deb( s, ... )
#endif

#ifdef WARNING_LOGGING_ON
#define warn( s, ... ) NSLog( @"Warning -- %@", [NSString stringWithFormat:(s), ##__VA_ARGS__] )
#else
#define warn( s, ... )
#endif

#ifdef ERROR_LOGGING_ON
#define err( s, ... ) NSLog( @"ERROR -- %@", [NSString stringWithFormat:(s), ##__VA_ARGS__] )
#else
#define err( s, ... )
#endif

#import <Foundation/Foundation.h>

@interface Debug : NSObject

+ (void) print_view_frame:(UIView *)view;
+ (void) print_frame:(CGRect)frame;
+ (void) print_point:(CGPoint)point;
+ (float) fake_waiting_duration;
+ (void) fakeWait;

@end
