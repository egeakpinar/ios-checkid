//
//  Debug.m
//  YAction
//
//  Created by Ismail Ege AKPINAR on 26/12/2012.
//  Copyright (c) 2013 i-DENTITY. All rights reserved.
//

#import "Debug.h"
#import "Helper.h"

@implementation Debug

+ (void) print_view_frame:(UIView *)view    {
    CGPoint origin = view.frame.origin;
    CGSize size = view.frame.size;
    logg(@"origin x:%f y:%f     size w:%f h:%f",origin.x,origin.y,size.width,size.height);
}

+ (void) print_frame:(CGRect)frame    {
    CGPoint origin = frame.origin;
    CGSize size = frame.size;
    logg(@"origin x:%f y:%f     size w:%f h:%f",origin.x,origin.y,size.width,size.height);
}

+ (void) print_point:(CGPoint)point {
    logg(@"x:%f\ty:%f",point.x,point.y);
}

+ (float) fake_waiting_duration   {
    if(DEBUG_MODE)  {
        return 0.0f;
    }
    else    {
        return 1.0f;
    }
}

+ (void) fakeWait   {
    BOOL j = YES;
    if(!DEBUG_MODE) {
        long max = 55555555;
        if(IS_IPHONE5)  {
            max = 999999999;
        }
        for(long i=0; i<max; i++)  {
            j = !j;
        }
    }
}

@end
