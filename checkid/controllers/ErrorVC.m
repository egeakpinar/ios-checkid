//
//  ErrorVC.m
//  checkid
//
//  Created by Ismail Ege AKPINAR on 05/03/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import "ErrorVC.h"
#import "CardCommunication.h"

@interface ErrorVC ()
@end

@implementation ErrorVC {
    BOOL is_in_transition;    // YES if nav controller is about to pop a VC
}



#pragma mark - Connection callbacks
- (void) ccStatusChange:(NSNotification *) notification {
    logg(@"+ ccStatusChange - ErrorVC");
    NSDictionary *dict = [notification userInfo];
    long card_status = [[dict objectForKey:@"card_status"] longValue];
    long reader_status = [[dict objectForKey:@"reader_status"] longValue];
    
    NSString *card_error_message=nil , *reader_error_message=nil;
    if(card_status == ID_CARD_CONNECT_ERROR)    {
        card_error_message = [dict objectForKey:@"card_status_error_message"];
    }
    if(reader_status == ID_READER_PROBLEM)    {
        reader_error_message = [dict objectForKey:@"reader_status_error_message"];
    }
    logg(@"reader %@ card %@", [CardCommunication readerStatusString:reader_status] ,
         [CardCommunication cardStatusString:card_status]);
    
    // Stay on this page until you get rid of the access error (doesn't mean you succeed to access)
    if(reader_status != ID_READER_PROBLEM)    {
        // Pop
        if(!is_in_transition) {
            is_in_transition = YES;
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}

#pragma mark - Default init methods

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    // Configure autoresizing masks
    [_img_question setAutoresizingMask:(UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin)];
    [_lbl_error setAutoresizingMask:(UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin)];
    
    // Localisation
    [_lbl_error setText:NSLocalizedString(@"Unsupported reader",nil)];
}

- (void) viewWillAppear:(BOOL)animated  {
    [super viewWillAppear:animated];
    
    self.title = NSLocalizedStringFromTable(@"CHECK-iD - Error",@"titles",nil);
    
    // Register for card notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(ccStatusChange:)
                                                 name:CCNOTIFICATION_STATUS_CHANGE
                                               object:nil];
    
    // Reset instance variables
    is_in_transition = NO;
}

- (void) viewWillDisappear:(BOOL)animated   {
    [super viewWillDisappear:animated];
    
    self.title = @"Back";
    
    // Unregister from card notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnDisconnectTouch:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewDidUnload {
    [self setImg_question:nil];
    [self setLbl_error:nil];
    [super viewDidUnload];
}
@end
