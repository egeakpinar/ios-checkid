//
//  StandbyVC.h
//  checkid
//
//  Created by Ismail Ege AKPINAR on 04/03/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VC.h"

@interface StandbyVC : VC

#pragma mark - Internal properties
@property (nonatomic, retain) VC *vc_info;
@property (nonatomic, retain) VC *vc_error;
@property (nonatomic, retain) NSOperation *operation_card_connection;   // Pointer to running operation, we will allow only one operation at a time
@property (nonatomic, retain) NSOperationQueue *queue_operation;    // Will move this to delegate level later

#pragma mark - UI elements
@property (unsafe_unretained, nonatomic) IBOutlet UIScrollView *scroll_main;
@property (unsafe_unretained, nonatomic) IBOutlet UIView *view_welcome;
@property (unsafe_unretained, nonatomic) IBOutlet UIView *view_waiting;
@property (unsafe_unretained, nonatomic) IBOutlet UIView *view_launch;
@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *img_launch_logo;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *btn_pair;

// Waiting view
@property (unsafe_unretained, nonatomic) IBOutlet UIActivityIndicatorView *activity_indicator;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *lbl_waiting;

// Welcome view
@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *img_logo;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *lbl_intro_message;
@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *img_status;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *lbl_status;
@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *img_status_animation;


- (IBAction)dummyTouch3:(id)sender;


#pragma mark - UI callbacks
- (IBAction)btnPairTouch:(id)sender;


@end
