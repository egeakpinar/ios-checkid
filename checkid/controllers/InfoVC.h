//
//  InfoVC.h
//  checkid
//
//  Created by Ismail Ege AKPINAR on 05/03/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VC.h"

@interface InfoVC : VC

#pragma mark - UI callbacks
- (IBAction)btnDisconnectTouch:(id)sender;
- (IBAction)btnPINTouch:(id)sender;

#pragma mark - UI elements
@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *img_profile;
@property (unsafe_unretained, nonatomic) IBOutlet UIView *view_shadow;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *lbl_title;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *lbl_valid;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *lbl_address;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *btn_pin;

@end
