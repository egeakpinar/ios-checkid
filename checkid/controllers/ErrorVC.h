//
//  ErrorVC.h
//  checkid
//
//  Created by Ismail Ege AKPINAR on 05/03/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import "VC.h"

@interface ErrorVC : VC

#pragma mark - UI callbacks
- (IBAction)btnDisconnectTouch:(id)sender;

#pragma mark - UI elements
@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *img_question;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *lbl_error;

@end
