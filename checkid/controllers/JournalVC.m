//
//  JournalVC.m
//  checkid
//
//  Created by Ismail Ege AKPINAR on 04/03/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import "JournalVC.h"

@interface JournalVC ()

@end

@implementation JournalVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [self.navigationItem setHidesBackButton:NO];
    
    UILabel *lbl_dummy = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 300, 100)];
    [lbl_dummy setText:@"Journal will be here"];
    [self.view addSubview:lbl_dummy];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
