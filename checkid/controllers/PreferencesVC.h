//
//  PreferencesVC.h
//  checkid
//
//  Created by Ismail Ege AKPINAR on 04/03/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VC.h"
#import "JournalVC.h"
#import "Cell_Switch.h"

@interface PreferencesVC : VC<UITableViewDataSource, UITableViewDelegate , Cell_Switch_Delegate>

#pragma mark - UI elements
@property (unsafe_unretained, nonatomic) IBOutlet UITableView *tbl_preferences;

#pragma mark - Internal properties
@property (nonatomic, retain) UINib *nib_cell_switch;
@property (nonatomic, retain) JournalVC *vc_journal;

@end
