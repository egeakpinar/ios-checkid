//
//  StandbyVC.m
//  checkid
//
//  Created by Ismail Ege AKPINAR on 04/03/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import "StandbyVC.h"
#import "PreferencesVC.h"
#import "InfoVC.h"
#import "ErrorVC.h"
#import "CardCommunication.h"

#import <QuartzCore/QuartzCore.h>

typedef enum    {
    animation_insert_reader,
    animation_reader_ok,
    animation_reader_error,
    animation_insert_card,
    animation_card_ok,
    animation_card_error
} animation_state;

@interface StandbyVC ()
@property (nonatomic, retain) CardReader *card_reader;
@property (nonatomic, retain) NSThread *thread_background;
@end

@implementation StandbyVC   {
    BOOL is_in_transition;   // YES if already pushing a VC onto the nav controller
}

// Synthesizing one property (otherwise first pragma mark ID doesn't show in XCode)
@synthesize scroll_main = _scroll_main;

#pragma mark - Convenience methods
- (void) animateLaunch  {
    // Align view
    float app_height = [UIScreen mainScreen].applicationFrame.size.height;
    float screen_height = [UIScreen mainScreen].bounds.size.height;
    float y_offset = (app_height - screen_height);  // Status bar height
    y_offset -= self.navigationController.navigationBar.frame.size.height;
    
    if(IS_IPAD)   {
        UIImage *img_launch_logo = [UIImage imageNamed:@"logo@2x.png"];
        float image_width = img_launch_logo.size.width;
        float image_height = img_launch_logo.size.height;
        
        // TODO: Check orientation here
        if([Helper isLandscapeOrientation])  {
            [_view_launch setFrame:CGRectMake(0, y_offset, [Helper screen_height] , [Helper screen_width] + abs(y_offset))];
            
            float x = ([Helper screen_height] - image_width)/2.0f;
            float y =([Helper screen_width] - image_height)/2.0f;
            y += y_offset;
            // I don't know why I need these, maybe something wrong with images
            y -= 26;
            [_img_launch_logo setFrame:CGRectMake(x,y, image_width, image_height)];
        }
        else    {
            [_view_launch setFrame:CGRectMake(0, y_offset, [Helper screen_width] , [Helper screen_height] + abs(y_offset))];
            
            UIImage *img_launch_logo = [UIImage imageNamed:@"logo@2x.png"];
            float image_width = img_launch_logo.size.width;
            float image_height = img_launch_logo.size.height;
            float x = ([Helper screen_width] - image_width)/2.0f;
            float y =([Helper screen_height] - image_height)/2.0f;
            y += y_offset;
            // I don't know why I need these, maybe something wrong with images
            x -= 8;
            y += 22;
            [_img_launch_logo setFrame:CGRectMake(x,y, image_width, image_height)];
        }


    }
    else    {
        [_view_launch setFrame:CGRectMake(0, y_offset, [Helper screen_width], [Helper screen_height] + abs(y_offset))];
    }
    
    // Custom easing animation (to move logo off the screen)
    NSUInteger const kNumFactors    = 10;
    CGFloat const kFactorsPerSec    = 15.0f;
    NSMutableArray* transforms = [NSMutableArray array];
    float factor = -1;
    for(NSUInteger i = 0; i < kNumFactors; i++)
    {
        CGFloat positionOffset  = factor;
        CATransform3D transform = CATransform3DMakeTranslation(0.0f, positionOffset, 0.0f);
        
        [transforms addObject:[NSValue valueWithCATransform3D:transform]];
        factor *=3;
    }
    CAKeyframeAnimation* animation = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
    animation.repeatCount           = 1;
    animation.duration              = kNumFactors * 1.0f/kFactorsPerSec;
    animation.fillMode              = kCAFillModeForwards;
    animation.values                = transforms;
    animation.removedOnCompletion   = NO; // final stage is equal to starting stage
    animation.autoreverses          = NO;
    animation.delegate = self;
    [animation setValue:@"upward" forKey:@"anim-id"];
    
    [_img_launch_logo.layer addAnimation:animation forKey:@"upward"];
    
    // Fade out
    [UIView transitionWithView:_view_launch duration:0.6f options:UIViewAnimationOptionCurveEaseIn animations:^{
        [_view_launch setAlpha:0.0f];
    } completion:^(BOOL finished) {
        [_view_launch setHidden:NO];
        [_view_launch removeFromSuperview];
    }];
}

- (void) showWaiting    {
    if(![NSThread isMainThread]) {
        [self performSelectorOnMainThread:@selector(showWaiting) withObject:nil waitUntilDone:NO];
        return;
    }
    float y_offset = _view_waiting.frame.size.height;
    
    // If device is tilted upside down, waiting animation should be inserted from top
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    if(orientation == UIInterfaceOrientationPortraitUpsideDown)  {
        y_offset *= -1;
        
        [_view_waiting setFrame:CGRectMake(0, -1 * _view_waiting.frame.size.height, _view_waiting.frame.size.width, _view_waiting.frame.size.height)]; // Waiting view should be placed right ABOVE the welcome screen

    }
    else    {
        [_view_waiting setFrame:CGRectMake(0, _view_welcome.frame.origin.y + _view_welcome.frame.size.height, _view_waiting.frame.size.width, _view_waiting.frame.size.height)]; // Waiting view should be placed right below the welcome screen
    }
    
    [_view_welcome setUserInteractionEnabled:NO];
    if(_scroll_main.contentOffset.y == 0)    {
        [_scroll_main setContentOffset:CGPointMake(0, y_offset) animated:YES];
    }

    [_activity_indicator startAnimating];    // Safer to force start it (even if it's already animating)
}

- (void) hideWaiting {
    if(![NSThread isMainThread]) {
        [self performSelectorOnMainThread:@selector(hideWaiting) withObject:nil waitUntilDone:NO];
        return;
    }

    [_view_welcome setUserInteractionEnabled:YES];
    if(_scroll_main.contentOffset.y != 0)   {
        [_scroll_main setContentOffset:CGPointMake(0, 0) animated:YES];
    }

    [_activity_indicator stopAnimating];    // Safer to force stop it (even if it's already stopped)
}

- (void) connectionSuccessful   {
    if(![NSThread isMainThread]) {
        [self performSelectorOnMainThread:@selector(connectionSuccessful) withObject:nil waitUntilDone:NO];
        return;
    }
    [self hideWaiting];
    if(!_vc_info)   {
        _vc_info = [[InfoVC alloc] initWithNibName:@"Info" bundle:nil];
    }
    
    if(!is_in_transition)    {
        is_in_transition = YES;
        [self.navigationController pushViewController:_vc_info animated:YES];
    }
}

- (void) connectionFailed   {
    if(![NSThread isMainThread]) {
        [self performSelectorOnMainThread:@selector(connectionFailed) withObject:nil waitUntilDone:NO];
        return;
    }
    
    [self hideWaiting];
    if(!_vc_error)   {
        _vc_error = [[ErrorVC alloc] initWithNibName:@"Error" bundle:nil];
    }
    
    if(!is_in_transition)    {
        is_in_transition = YES;
        [self.navigationController pushViewController:_vc_error animated:YES];
    }

}

// Updates UI based on state information
- (void) updateUIStatesWithReaderStatus:(long) reader_status readerErrorMessage:(NSString *) reader_error_message cardStatus:(long) card_status cardErrorMessages:(NSString *) card_error_message  {
    logg(@"updating UI reader %@ card %@", [CardCommunication readerStatusString:reader_status] , [CardCommunication cardStatusString:card_status]);
    
    animation_state state;
    BOOL should_examine_card = YES;
    switch (reader_status) {
        case ID_READER_ABSENT:
            should_examine_card = NO;
            [_lbl_status setText:NSLocalizedString(@"Waiting for reader",nil)];
            state = animation_insert_reader;
            [self hideWaiting];
            
            break;
        case ID_READER_PROBLEM:
            // Go to error screen
            [self connectionFailed];
            state = animation_reader_error;
            
            break;
        case ID_READER_ACCESS_SUCCESSFUL:
            // Look at card status
            
            break;
        case ID_READER_PRESENT:
        case ID_READER_TRYING:
            should_examine_card = NO;
            [_lbl_status setText:NSLocalizedString(@"Reader detected, working",)];
            state = animation_reader_ok;
            [self showWaiting];
            
            break;
        default:
            break;
    }
    if(should_examine_card) {
        switch (card_status) {
            case ID_CARD_PRESENT:
                [self showWaiting];
                state = animation_card_ok;
                [_lbl_status setText:NSLocalizedString(@"Card detected, working",)];
                
                break;
            case ID_CARD_ABSENT:
                [self hideWaiting];   // debb
                state = animation_insert_card;
                [_lbl_status setText:NSLocalizedString(@"Waiting for card",)];
                
                break;
            case ID_CARD_CONNECT_ERROR:
                if(card_error_message)
                    [_lbl_status setText:[NSString stringWithFormat:@"%@ (%@)", NSLocalizedString(@"Error reading card", nil),card_error_message]];
                else
                    [_lbl_status setText:NSLocalizedString(@"Error reading card",nil)];
                state = animation_card_error;
                [self hideWaiting];
                
                break;
            case ID_CARD_ACCESS_SUCCESSFUL:
                // Go to info screen
                state = animation_card_ok;
                [self connectionSuccessful];
                
                break;
            default:
                break;
        }
    }
    
    [self updateStateAnimation:state];
    
    
    // Show pairing button if BAI reader detected
    if([[CardCommunication instance] reader] && [[CardCommunication instance] reader].type == bai && reader_status != ID_READER_ABSENT) {
        [_btn_pair setHidden:NO];
    }
    else    {
        [_btn_pair setHidden:YES];
    }    
}


// Note: This method is buggy when called before view is loaded (call it in viewDidAppear)
- (void) updateStateAnimation:(animation_state) state   {
    [_img_status_animation setAnimationDuration:2.0f];
    
    [_img_status_animation setAnimationImages:nil];
    [_img_status_animation setImage:nil];
    
    if(!IS_IPAD) {
        switch (state) {
            case animation_insert_reader:
                [_img_status_animation setAnimationImages:[NSArray arrayWithObjects:
                                                           [UIImage imageNamed:@"iphone-insertion-reader-1"],
                                                           [UIImage imageNamed:@"iphone-insertion-reader-2"],
                                                           [UIImage imageNamed:@"iphone-insertion-reader-3"],
                                                           [UIImage imageNamed:@"iphone-insertion-reader-3"],
                                                           [UIImage imageNamed:@"iphone-insertion-reader-3"],nil]];
                [_img_status_animation startAnimating];
                
                break;
            case animation_reader_ok:
                [_img_status_animation setImage:[UIImage imageNamed:@"iphone-insertion-reader-4"]];
                [_img_status_animation stopAnimating];
                
                break;
            case animation_reader_error:
                [_img_status_animation setAnimationImages:[NSArray arrayWithObjects:
                                                 [UIImage imageNamed:@"iphone-reader-error-2"],
                                                 [UIImage imageNamed:@"iphone-reader-error-4"],
                                                 nil]];
                [_img_status_animation startAnimating];
                
                break;
            case animation_insert_card:
                [_img_status_animation setAnimationImages:[NSArray arrayWithObjects:
                                                           [UIImage imageNamed:@"iphone-insertion-card-1"],
                                                           [UIImage imageNamed:@"iphone-insertion-card-2"],
                                                           [UIImage imageNamed:@"iphone-insertion-card-3"],
                                                           [UIImage imageNamed:@"iphone-insertion-card-3"],
                                                           [UIImage imageNamed:@"iphone-insertion-card-3"],nil]];
                [_img_status_animation startAnimating];
                
                break;
            case animation_card_ok:
                [_img_status_animation setImage:[UIImage imageNamed:@"iphone-insertion-card-4"]];
                [_img_status_animation stopAnimating];
                
                break;
            case animation_card_error:
                [_img_status_animation setAnimationImages:[NSArray arrayWithObjects:
                                                           [UIImage imageNamed:@"iphone-card-error-2"],
                                                           [UIImage imageNamed:@"iphone-card-error-4"],
                                                           nil]];
                [_img_status_animation startAnimating];
                
                break;
            default:
                warn(@"Unrecognised animation state %d", state);
                break;
        }
    }
    else    {
        switch (state) {
            case animation_insert_reader:
                [_img_status_animation setAnimationImages:[NSArray arrayWithObjects:
                                                           [UIImage imageNamed:@"ipad-insertion-reader-1"],
                                                           [UIImage imageNamed:@"ipad-insertion-reader-2"],
                                                           [UIImage imageNamed:@"ipad-insertion-reader-3"],
                                                           [UIImage imageNamed:@"ipad-insertion-reader-3"],
                                                           [UIImage imageNamed:@"ipad-insertion-reader-3"],nil]];
                [_img_status_animation startAnimating];
                
                break;
            case animation_reader_ok:
                [_img_status_animation setImage:[UIImage imageNamed:@"ipad-insertion-reader-4"]];
                [_img_status_animation stopAnimating];
                
                break;
            case animation_reader_error:
                [_img_status_animation setAnimationImages:[NSArray arrayWithObjects:
                                                           [UIImage imageNamed:@"ipad-reader-error-2"],
                                                           [UIImage imageNamed:@"ipad-reader-error-4"],
                                                           nil]];
                [_img_status_animation startAnimating];
                
                break;
            case animation_insert_card:
                [_img_status_animation setAnimationImages:[NSArray arrayWithObjects:
                                                           [UIImage imageNamed:@"ipad-insertion-card-1"],
                                                           [UIImage imageNamed:@"ipad-insertion-card-2"],
                                                           [UIImage imageNamed:@"ipad-insertion-card-3"],
                                                           [UIImage imageNamed:@"ipad-insertion-card-3"],
                                                           [UIImage imageNamed:@"ipad-insertion-card-3"],nil]];
                [_img_status_animation startAnimating];
                
                break;
            case animation_card_ok:
                [_img_status_animation setImage:[UIImage imageNamed:@"ipad-insertion-card-4"]];
                [_img_status_animation stopAnimating];
                
                break;
            case animation_card_error:
                [_img_status_animation setAnimationImages:[NSArray arrayWithObjects:
                                                           [UIImage imageNamed:@"ipad-card-error-2"],
                                                           [UIImage imageNamed:@"ipad-card-error-4"],
                                                           nil]];
                [_img_status_animation startAnimating];
                
                break;
            default:
                warn(@"Unrecognised animation state %d", state);
                break;
        }
    }
}

#pragma mark - UI callbacks

// Touch callback for view_waiting
// Called through both pan and tap recognisers
- (void) viewWaitingTouch:(UIGestureRecognizer *) sender   {
    if([sender state] == UIGestureRecognizerStateEnded) {
        [_activity_indicator stopAnimating];
        [_scroll_main setContentOffset:CGPointMake(0, 0) animated:YES];
        [_view_welcome setUserInteractionEnabled:YES];
        
        if(_operation_card_connection)  {
            [_operation_card_connection cancel];
        }
    }
}

- (IBAction)btnPairTouch:(id)sender {
    [[[CardCommunication instance] reader] showPairingGUI:self.navigationController callback:pairingCallback];
}

// TODO: Show a popup here
void pairingCallback(BOOL successful)   {
    if(successful)  {
        logg(@"pairing successful");
    }
    else    {
        warn(@"pairing failed");
    }
}

#pragma mark - Default init methods

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    logg(@"+ viewDidLoad");
    self.title = NSLocalizedStringFromTable(@"CHECK-iD",@"titles",nil);
    [_scroll_main setScrollEnabled:NO];
    [_scroll_main setClipsToBounds:YES];
    
    // Add gesture recognisers for dismissing activity
    UITapGestureRecognizer *recogniser_tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewWaitingTouch:)];
    [recogniser_tap setNumberOfTapsRequired:1];
    [recogniser_tap setNumberOfTouchesRequired:1];
    [_view_waiting addGestureRecognizer:recogniser_tap];
    
    UIPanGestureRecognizer *recogniser_pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(viewWaitingTouch:)];
    [recogniser_pan setMinimumNumberOfTouches:1];
    [_view_waiting addGestureRecognizer:recogniser_pan];
    

    
    _queue_operation = [[NSOperationQueue alloc] init];
    [_queue_operation setMaxConcurrentOperationCount:1];
    
    // Autoresizing masks
    [_img_logo setAutoresizingMask:(UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin)];
    [_lbl_intro_message setAutoresizingMask:(UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth)];
    [_img_status setAutoresizingMask:(UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin)];
    [_lbl_status setAutoresizingMask:(UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin)];
    [_img_status_animation setAutoresizingMask:(UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin)];
    [_btn_pair setAutoresizingMask:(UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleTopMargin)];

    
    // Animate launch transition
    [self animateLaunch];
    
    // Localisation
    [_lbl_intro_message setText:NSLocalizedString(@"CHECK-iD is a mobile security app with multi reader and multi smartcard support. It will automatically detect when you insert a smartcard reader.", nil)];
    [_lbl_waiting setText:NSLocalizedString(@"Working, touch here to cancel", nil)];

}

- (void) viewWillAppear:(BOOL)animated  {
    [super viewWillAppear:animated];

    // Register for card notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(ccStatusChange:)
                                                 name:CCNOTIFICATION_STATUS_CHANGE
                                               object:nil];
    

    // NOTE: This might be disabled when going to preferences screen
    [_view_welcome setUserInteractionEnabled:YES];
    
    // Init instance variables
    is_in_transition = NO;
}

- (void) viewDidAppear:(BOOL)animated   {
    [super viewDidAppear:animated];
    
    // Update UI based on latest status (notifications before this view are captured by the previous VC)
    CardCommunication *cc = [CardCommunication instance];
    [self updateUIStatesWithReaderStatus:cc.reader_status readerErrorMessage:cc.reader_status_error_message cardStatus:cc.card_status cardErrorMessages:cc.card_status_error_message];
}

- (void) viewWillDisappear:(BOOL)animated   {
    [super viewWillDisappear:animated];
    // Unregister from card notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void) configureFrames  {
    [_scroll_main setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [_view_welcome setFrame:CGRectMake(0, 0, _scroll_main.frame.size.width, _scroll_main.frame.size.height)];
    [_view_waiting setFrame:CGRectMake(0, _view_welcome.frame.origin.y + _view_welcome.frame.size.height, self.view.frame.size.width, 60)]; // Waiting view should be placed right where welcome screen ends (Note that this frame is re-adjusted in showWaiting method)
}

#pragma mark - Connection callbacks
- (void) ccStatusChange:(NSNotification *) notification {
    logg(@"+ ccStatusChange - StandbyVC");
    
    NSDictionary *dict = [notification userInfo];
    long card_status = [[dict objectForKey:@"card_status"] unsignedIntValue];
    long reader_status = [[dict objectForKey:@"reader_status"] unsignedIntValue];
    
    NSString *card_error_message=nil , *reader_error_message=nil;
    if(card_status == ID_CARD_CONNECT_ERROR)    {
        card_error_message = [dict objectForKey:@"card_status_error_message"];
    }
    if(reader_status == ID_READER_PROBLEM)    {
        reader_error_message = [dict objectForKey:@"reader_status_error_message"];
    }
    
    logg(@"reader %@ card %@", [CardCommunication readerStatusString:reader_status] ,
         [CardCommunication cardStatusString:card_status]);
    
    [self updateUIStatesWithReaderStatus:reader_status readerErrorMessage:reader_error_message cardStatus:card_status cardErrorMessages:card_error_message];
}

#pragma mark - Default methods

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setScroll_main:nil];
    [self setView_welcome:nil];
    [self setView_waiting:nil];
    [self setActivity_indicator:nil];
    [self setLbl_intro_message:nil];
    [self setImg_logo:nil];
    [self setImg_status:nil];
    [self setLbl_status:nil];
    [self setView_launch:nil];
    [self setImg_launch_logo:nil];
    [self setImg_status_animation:nil];
    [self setBtn_pair:nil];
    [self setLbl_waiting:nil];
    [super viewDidUnload];
}



@end
