//
//  PreferencesVC.m
//  checkid
//
//  Created by Ismail Ege AKPINAR on 04/03/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import "PreferencesVC.h"
#import "Cell_Switch.h"

@interface PreferencesVC ()

@end

@implementation PreferencesVC

#pragma mark - UICallbacks
- (void) navbarRightTouch:(id)sender    {
    [self dismissModalViewControllerAnimated:YES];
}

#pragma mark - UITableViewDataSource callbacks
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView   {
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section    {
    if(section == 0)    {
        // View journal
        return 1;
    }
    else if(section == 1)   {
        // Settings
        return 2;
    }
    else if (section ==2)   {
        // About
        return 2;
    }
    warn(@"Unexpected section index for table");
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath  {
    int section = indexPath.section;
    int row = indexPath.row;
    
    if(section == 0)    {
        if(row == 0)    {
            // View journal cell
            // NOTE: Cell style and cell.textLabel are iOS3.0 and later

            UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell_journal"];
            [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
            [cell.textLabel setText:NSLocalizedStringFromTable(@"View Journal",@"preferences",nil)];
            return cell;
        }
    }
    else if(section == 1)   {
        // Cells with switch

        NSArray *topLevelItems = [_nib_cell_switch instantiateWithOwner:self options:nil];
        Cell_Switch *cell_switch = [topLevelItems objectAtIndex:0];
        [cell_switch setDelegate:self];
        if(!cell_switch)   {
            err(@"Failed to initialise cell - Cell_Switch");
        }
        if(row == 0)    {
            [cell_switch.lbl setText:NSLocalizedStringFromTable(@"Always request PIN",@"preferences",nil)];
        }
        else if(row == 1)   {
            [cell_switch.lbl setText:NSLocalizedStringFromTable(@"Disable dim screen",@"preferences", nil)];
        }
        return cell_switch;
    }
    else if(section == 2)   {
        UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell_journal"];
        if(row == 0)    {
            [cell.textLabel setText:NSLocalizedStringFromTable(@"Reader Information",@"preferences",nil)];
        }
        else if(row == 1)   {
            [cell.textLabel setText:NSLocalizedStringFromTable(@"License",@"preferences",nil)];
        }
        else    {
            warn(@"Returning nil cell for table, section %d row %d", indexPath.section, indexPath.row);
            return nil;
        }
        return cell;
    }
    warn(@"Returning nil cell for table, section %d row %d", indexPath.section, indexPath.row);
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section   {
    if(section == 0)    {
        return 90.0f;   // Top margin
    }
    return 0;
}

#pragma mark - UITableViewDelegate methods
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    int section = indexPath.section;
    int row = indexPath.row;
    
    if(section == 0)    {
        if(row == 0)    {
            // View journal
            if(!_vc_journal)    {
                _vc_journal = [[JournalVC alloc] init];
            }
            [self.navigationController pushViewController:_vc_journal animated:YES];
        }
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

#pragma mark - Cell_Switch_Delegate methods

- (void)cellSwitch:(Cell_Switch *)cell valueChanged:(id)sender  {
    logg(@"cell value changed to %d", cell.switchOnOff.on);
}

#pragma mark - Default init methods
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.title = NSLocalizedStringFromTable(@"CHECK-iD Settings",@"titles",nil);
    
    if(!_nib_cell_switch)   {
        _nib_cell_switch = [UINib nibWithNibName:@"Cell_Switch" bundle:[NSBundle mainBundle]];
    }
    
    self.navigationItem.rightBarButtonItems = nil;
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(navbarRightTouch:)];
}

- (void) configureFrames    {
    [_tbl_preferences setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
}

#pragma mark - Default methods
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setTbl_preferences:nil];
    [super viewDidUnload];
}
@end
