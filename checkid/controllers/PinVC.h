//
//  PinVC.h
//  checkid
//
//  Created by Ismail Ege AKPINAR on 06/03/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import "VC.h"

@interface PinVC : VC<UITextFieldDelegate>

#pragma mark - UI elements
@property (unsafe_unretained, nonatomic) IBOutlet UITextField *txt_pin;

#pragma mark - UI callbacks
- (IBAction)dummyTouch1:(id)sender;

@end
