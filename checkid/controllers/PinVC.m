//
//  PinVC.m
//  checkid
//
//  Created by Ismail Ege AKPINAR on 06/03/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import "PinVC.h"

@interface PinVC ()
@property (nonatomic, assign) BOOL is_validating;
@end

#define MIN_PIN_LENGTH 4
@implementation PinVC

#pragma mark - Notification observer methods

- (void) textFieldTextDidChange:(NSNotification *)notification {
    NSString *final_input = _txt_pin.text;
    if(final_input && [final_input length] >= MIN_PIN_LENGTH) {
        // Allow submission
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(navbarRightTouch:)];
    }
    else    {
        // Disallow submission
        self.navigationItem.rightBarButtonItem = nil;
    }
}

#pragma mark - Convenience methods

- (void) submitPIN    {
    if(_is_validating)  {
        return;
    }
    _is_validating = YES;

    NSString *string_input = _txt_pin.text;
    if(!string_input || [string_input length]< MIN_PIN_LENGTH)    {
        [Helper displayMessageWithTitle:@"Error" message:@"Entered PIN must be at least 4 characters/digits" delegate:nil tag:-1 buttonTitle:nil];
        _is_validating = NO;
    }
    else    {
        [Helper show_activity_indicator];        
        [self performSelectorInBackground:@selector(validatePIN:) withObject:string_input];
    }
}

// Background task to validate PIN
- (void) validatePIN:(NSString *) input    {
    BOOL is_valid = NO;
    
    [Debug fakeWait];
    if([input isEqualToString:@"1111"])  {
        is_valid = YES;
    }
    _is_validating = NO;
    [Helper hide_activity_indicator];
    if(is_valid)    {
        [self performSelectorOnMainThread:@selector(PINvalid) withObject:nil waitUntilDone:YES];
    }
    else    {
        [self performSelectorOnMainThread:@selector(PINinvalid) withObject:nil waitUntilDone:YES];
    }    
}

- (void) PINvalid   {
    [self.navigationController popViewControllerAnimated:YES];
    [Helper displayMessageWithTitle:@"Successful" message:@"PIN validated" delegate:nil tag:-1 buttonTitle:nil];
}

- (void) PINinvalid {
    [_txt_pin setText:@""]; // Reset input
    [Helper displayMessageWithTitle:@"Error" message:@"Entered PIN is incorrect, please try again" delegate:nil tag:-1 buttonTitle:nil];
}

#pragma mark - UI callbacks

// Overriding
- (void) navbarRightTouch:(id)sender    {
    [self submitPIN];
}


- (IBAction)dummyTouch1:(id)sender {
    if(_txt_pin.keyboardType == UIKeyboardTypeDefault)  {
        [_txt_pin setKeyboardType:UIKeyboardTypeDecimalPad];
        [_txt_pin resignFirstResponder];
        [_txt_pin becomeFirstResponder];
    }
    else    {
        [_txt_pin setKeyboardType:UIKeyboardTypeDefault];
        [_txt_pin resignFirstResponder];
        [_txt_pin becomeFirstResponder];
    }

    [self textFieldTextDidChange:nil];
}

#pragma mark - UITextFieldDelegate methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField  {
    [self submitPIN];
    return NO;
}


#pragma mark - Default init methods
-(void) viewWillAppear:(BOOL)animated   {
    [super viewWillAppear:animated];
    [_txt_pin setText:@""];
    [_txt_pin setEnablesReturnKeyAutomatically:YES];
    [_txt_pin becomeFirstResponder];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];    
	// Do any additional setup after loading the view.
    
    self.title = NSLocalizedStringFromTable(@"Enter PIN code",@"titles",nil);
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(navbarRightTouch:)];
    self.navigationItem.rightBarButtonItem = nil;
    [self.navigationItem setHidesBackButton:NO animated:YES];
        
    // Attach an observer for text change event
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldTextDidChange:) name:@"UITextFieldTextDidChangeNotification" object:_txt_pin];
    
    // Configure autoresizing masks
    [_txt_pin setAutoresizingMask:(UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin)];
    
    // UI configuration
    [_txt_pin setClearButtonMode:UITextFieldViewModeWhileEditing];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setTxt_pin:nil];
    [super viewDidUnload];
}
@end
