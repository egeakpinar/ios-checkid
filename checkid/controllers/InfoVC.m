//
//  InfoVC.m
//  checkid
//
//  Created by Ismail Ege AKPINAR on 05/03/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import "InfoVC.h"
#import "PinVC.h"
#import "CardCommunication.h"

@interface InfoVC ()
@property (nonatomic, assign) BOOL is_img_fullscreen;   // YES if profile image is full screen
@property (nonatomic, retain) PinVC *vc_pin;

@end

@implementation InfoVC  {
    BOOL is_popping;    // YES if nav controller is about to pop a VC
}

#pragma mark - UI callbacks
- (IBAction)btnPINTouch:(id)sender {
    // debb
    CardCommunication *cc = [CardCommunication instance];
    CardReader *reader = [cc reader];
    Card *card = reader.card;
    if(!card)   {
        err(@"card is nil");
        return;
    }

    if(reader.type != feitian)  {
        NSThread *thread_sdk = [[CardCommunication instance] thread_background];
        if(reader.type != bai)  {
            [reader performSelector:@selector(powerOnCard) onThread:thread_sdk withObject:nil waitUntilDone:YES];
            [card performSelector:@selector(verifyPIN:) onThread:thread_sdk withObject:@"1234" waitUntilDone:YES];
            [reader performSelector:@selector(powerOffCard) onThread:thread_sdk withObject:nil waitUntilDone:YES];
        }
        else    {
            [reader performSelector:@selector(powerOnCard) onThread:thread_sdk withObject:nil waitUntilDone:NO];
        }
        
        return;
    }
    else    {   // Feitian requires everything on main thread
        
        long resp = [reader powerOnCard];
        if(resp == ID_OK)    {
            deb(@"power ON successful, will verifyPIN now");
            long response = [card verifyPIN:@"1234"];
            deb(@"verifyPIN response %@", [CardCommunication responseString:response]);
            
            // TODO: Should it power on/off for each request?
        }
        else    {
            deb(@"failed to power on");
        }
        
        resp = [reader powerOffCard];
    }
    

    // debb
//    [self showPINPad];
}


#pragma mark - Convenience methods
// Displays keypad for PIN input
- (void) showPINPad {
    if(!_vc_pin)    {
        _vc_pin = [[PinVC alloc] initWithNibName:@"PIN" bundle:[NSBundle mainBundle]];
    }
    [self.navigationController pushViewController:_vc_pin animated:YES];
}

// Displays profile picture in full width
- (void) showFullImage:(UITapGestureRecognizer *) recogniser  {
    if(_is_img_fullscreen)  {
        [UIView animateWithDuration:0.4 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
            _img_profile.transform = CGAffineTransformIdentity;
            [_view_shadow setAlpha:0.0f];
        } completion:^(BOOL completed)  {
            _is_img_fullscreen = NO;
        }];

    }
    else    {
        float scale = 3.0f;
        if([Helper isLandscapeOrientation]) {
            scale = 2.4f;
        }
        float delta_height = _img_profile.frame.size.height * (scale-1);
        CGAffineTransform tr = CGAffineTransformScale(_img_profile.transform, scale, scale);
        tr = CGAffineTransformTranslate(tr, 0, (delta_height/scale)/2.0f);
        [UIView animateWithDuration:0.4 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
            _img_profile.transform = tr;
            [_view_shadow setAlpha:1.0f];
        } completion:^(BOOL completed)  {
            _is_img_fullscreen = YES;
        }];
    }
}


#pragma mark - Connection callbacks
- (void) ccStatusChange:(NSNotification *) notification {
    NSDictionary *dict = [notification userInfo];
    long card_status = [[dict objectForKey:@"card_status"] unsignedIntValue];
    long reader_status = [[dict objectForKey:@"reader_status"] unsignedIntValue];
    
    NSString *card_error_message=nil , *reader_error_message=nil;
    if(card_status == ID_CARD_CONNECT_ERROR)    {
        card_error_message = [dict objectForKey:@"card_status_error_message"];
    }
    if(reader_status == ID_READER_PROBLEM)    {
        reader_error_message = [dict objectForKey:@"reader_status_error_message"];
    }
    
    // TODO: You might want to be less strict here
    if(card_status == ID_CARD_ACCESS_SUCCESSFUL && reader_status == ID_READER_ACCESS_SUCCESSFUL)    {
        // Stay on this page
    }
    else    {
        if(!is_popping) {
            is_popping = YES;
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}

#pragma mark - Default init methods
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = NSLocalizedStringFromTable(@"CHECK-iD - Connected",@"titles",nil);
        
        // Initialize private properties
        _is_img_fullscreen = NO;

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    UITapGestureRecognizer *recogniser_tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showFullImage:)];
    [recogniser_tap setNumberOfTapsRequired:1];
    [recogniser_tap setNumberOfTouchesRequired:1];
    [_img_profile setUserInteractionEnabled:YES];
    [_img_profile addGestureRecognizer:recogniser_tap];
    
    [_view_shadow setAlpha:0.0f];
    
    // Configure autoresizing masks
    [_img_profile setAutoresizingMask:(UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin)];
    [_lbl_title setAutoresizingMask:(UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin)];
    [_lbl_valid setAutoresizingMask:(UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin)];
    [_lbl_address setAutoresizingMask:(UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin)];
    [_btn_pin setAutoresizingMask:(UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin |  UIViewAutoresizingFlexibleBottomMargin)];
    
    UITapGestureRecognizer *recogniser_tap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showFullImage:)];
    [recogniser_tap2 setNumberOfTapsRequired:1];
    [recogniser_tap2 setNumberOfTouchesRequired:1];
    [_view_shadow addGestureRecognizer:recogniser_tap2];
    
    // Localisation
    [_btn_pin setTitle:NSLocalizedString(@"PIN not verified, click to verify PIN", nil) forState:UIControlStateNormal];
    
}

- (void) viewWillDisappear:(BOOL)animated    {
    [super viewWillDisappear:animated];
    self.title = @"Back";
    
    // Unregister from card notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void) viewWillAppear:(BOOL)animated  {
    [super viewWillAppear:animated];
    self.title = @"CHECK-iD - Connected";

    // Register for card notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(ccStatusChange:)
                                                 name:CCNOTIFICATION_STATUS_CHANGE
                                               object:nil];
    
    // Initialise instance variables
    is_popping = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setImg_profile:nil];
    [self setView_shadow:nil];
    [self setLbl_title:nil];
    [self setLbl_valid:nil];
    [self setLbl_address:nil];
    [self setBtn_pin:nil];
    [super viewDidUnload];
}
@end
