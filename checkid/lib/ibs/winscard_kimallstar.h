//
//  winscard_ibs.h
//  checkid
//
//  Created by Ismail Ege AKPINAR on 19/03/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#ifndef checkid_winscard_ibs_h
#define checkid_winscard_ibs_h

LONG SCa02EstablishContext(DWORD dwScope, LPCVOID pvReserved1,
                           LPCVOID pvReserved2, LPSCARDCONTEXT phContext);
LONG SCa02IsValidContext(SCARDCONTEXT hContext);

LONG SCa02ReleaseContext(SCARDCONTEXT hContext);


LONG SCa02SetTimeout(SCARDCONTEXT hContext, DWORD dwTimeout);

LONG SCa02Connect(SCARDCONTEXT hContext, LPCSTR szReader,
                  DWORD dwShareMode, DWORD dwPreferredProtocols, LPSCARDHANDLE phCard,
                  LPDWORD pdwActiveProtocol);

LONG SCa02Reconnect(SCARDHANDLE hCard, DWORD dwShareMode,
                    DWORD dwPreferredProtocols, DWORD dwInitialization,
                    LPDWORD pdwActiveProtocol);

LONG SCa02Disconnect(SCARDHANDLE hCard, DWORD dwDisposition);

LONG SCa02BeginTransaction(SCARDHANDLE hCard);

LONG SCa02EndTransaction(SCARDHANDLE hCard, DWORD dwDisposition);

LONG SCa02CancelTransaction(SCARDHANDLE hCard);

LONG SCa02Status(SCARDHANDLE hCard, LPSTR mszReaderNames,
                 LPDWORD pcchReaderLen, LPDWORD pdwState,
                 LPDWORD pdwProtocol, LPBYTE pbAtr, LPDWORD pcbAtrLen);

LONG SCa02GetStatusChange(SCARDCONTEXT hContext, DWORD dwTimeout,
                          LPSCARD_READERSTATE_A rgReaderStates, DWORD cReaders);

LONG SCa02Control(SCARDHANDLE hCard, DWORD dwControlCode,
                  const void *pbSendBuffer, DWORD cbSendLength,
                  void *pbRecvBuffer, DWORD cbRecvLength, LPDWORD lpBytesReturned);
LONG SCa02Transmit(SCARDHANDLE hCard, LPCSCARD_IO_REQUEST pioSendPci,
                   LPCBYTE pbSendBuffer, DWORD cbSendLength,
                   LPSCARD_IO_REQUEST pioRecvPci, LPBYTE pbRecvBuffer,
                   LPDWORD pcbRecvLength);
LONG SCa02SecTransmit(SCARDHANDLE hCard,LPCBYTE pbSendBuffer, DWORD cbSendLength,LPBYTE pbRecvBuffer,LPDWORD pcbRecvLength);
LONG SCa02ListReaderGroups(DWORD hContext,
                           LPCBYTE *mszGroups, LPDWORD *pcchGroups);

LONG SCa02ListReaders(SCARDCONTEXT hContext, LPCSTR mszGroups,
                      LPSTR mszReaders, LPDWORD pcchReaders);

LONG SCa02Cancel(SCARDCONTEXT hContext);

#define SCARD_PCI_T0	(&g_rgSCa02T0Pci) /**< protocol control information (PCI) for T=0 */
#define SCARD_PCI_T1	(&g_rgSCa02T1Pci) /**< protocol control information (PCI) for T=1 */
#define SCARD_PCI_RAW	(&g_rgSCa02RawPci) /**< protocol control information (PCI) for RAW protocol */

extern SCARD_IO_REQUEST g_rgSCa02T0Pci, g_rgSCa02T1Pci, g_rgSCa02RawPci;


#endif
