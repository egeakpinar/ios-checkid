//
//  CardCommunication.m
//  checkid
//
//  Created by Ismail Ege AKPINAR on 14/03/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import "CardCommunication.h"
#import "Debug.h"
#import "Helper.h"

#import <ExternalAccessory/ExternalAccessory.h>

#import <pthread.h>
#import <objc/runtime.h>

@interface CardCommunication()


@property (nonatomic, assign) pthread_t pthread_background;    // pthread pointer to thread_background
@property (nonatomic, retain) NSThread *thread_current; // Reference to current thread (this is not main thread)
@property NSMutableArray *arr_timed_notifications;  // This array is for timed state change notifications to be posted
@property(nonatomic, retain) NSTimer *timer_poll_status;  // Timer used to poll status

// EAAccessory notifications should be handled by this thread instead
@property NSMutableArray *arr_ea_notifications; // This array holds EAAccessory notifications received on main thread
@property NSThread *notificationThread;
@property NSLock *notificationLock;
@property NSMachPort *notificationPort;

// Timeout methods

@property (nonatomic, assign) int timeout_counter;
@property (nonatomic, retain) NSLock *lock_timeout_counter;

@end

@implementation CardCommunication



// Singleton instance
static CardCommunication *_instance;
+ (id) instance {
    if(!_instance)  {
        _instance = [[CardCommunication alloc] init];
        // Set to defaults
        [_instance setIs_pooled_notification_mode:YES];
        [_instance setIs_timeout_enabled:YES];
    }
    return _instance;
}

#pragma mark - Thread control methods

// Should be started only once during application runtime
- (void) start  {
    @autoreleasepool {
        // Register for accessory notifications
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(accessoryDidConnect:) name:EAAccessoryDidConnectNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(accessoryDidDisconnect:) name:EAAccessoryDidDisconnectNotification object:nil];
        
        [[EAAccessoryManager sharedAccessoryManager] registerForLocalNotifications];
        
        // Set to initial status
        [self setReader_status:ID_READER_ABSENT];
        [self setCard_status:ID_CARD_ABSENT];
        
        
        
        // Initialise instance variables
        _arr_timed_notifications = [[NSMutableArray alloc] init];
        _thread_current = [NSThread currentThread];
        _lock_timeout_counter = [[NSLock alloc] init];
        _timeout_counter = 0;
        
        [self setupThreading];
        
        // Check if an accessory is already connected
        // (Note that this must be executed after setupThreading finishes, to be able to
        // process EA notifications properly)
        // (Note: It's safer to trigger this slightly after app launch, otherwise EA sometimes fails to read protocol string)
        [self performSelector:@selector(tryDetectAlreadyConnectedAccessory) withObject:nil afterDelay:2.0f];
        
        NSTimer *timer_check;
        if(_is_timeout_enabled) {
            // Setup a signal handler for our custom signal (will be used to terminate thread in case of timeout)
            signal(SIGUSR1, signalHandler);
                    
            // Add a timer to check timeout
            timer_check = [NSTimer scheduledTimerWithTimeInterval:CC_POLLING_PERIOD_S * 10 target:self selector:@selector(checkTimeOut) userInfo:nil repeats:YES];
        }

        // Loop infinitely
        do  {
            // Start the run loop but return after each source is handled.
            SInt32    result = CFRunLoopRunInMode(kCFRunLoopDefaultMode, CC_POLLING_PERIOD_S * 10, YES);
            
            // If a source explicitly stopped the run loop, or if there are no
            // sources or timers, go ahead and exit.
            if ((result == kCFRunLoopRunStopped) || (result == kCFRunLoopRunFinished))
                break;
            else if([[NSThread currentThread] isCancelled]) {
                warn(@"CardCommunication thread cancelled");
                break;
            }
            
            // Check for any other exit conditions here and set the
            // done variable as needed.
        }
        while (true);

        if(timer_check) {
            [timer_check invalidate];
        }
        
        warn(@"Terminating CardCommunication thread");
    }
}

- (void) stop   {
    // Unregister from accessory notifications
    [[EAAccessoryManager sharedAccessoryManager] unregisterForLocalNotifications];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    // If there's still connected accessories, call their delegate methods
    // This prevents a bug with iBankSpace (and possibly with other readers) that try to access
    // their thread after it's cancelled
    EAAccessoryManager *manager = [EAAccessoryManager sharedAccessoryManager];
    for(EAAccessory *acc in [manager connectedAccessories])    {
        if(acc.delegate && [acc.delegate respondsToSelector:@selector(accessoryDidDisconnect:)])    {
            [acc.delegate accessoryDidDisconnect:acc];
        }
    }
        
    // Stop thread (if active)
    if(_thread_background && [_thread_background isExecuting])  {
        [_thread_background cancel];
    }
    
    // Remove threading
    [self removeThreading];
    
    // Kill self thread (if separate than main thread)
    if(_thread_current != [NSThread mainThread])    {
        [_thread_current cancel];
    }
}

// Opens a MachPort to receive notifications received on the main thread
// (we want to process on this thread instead but by default, they get delivered to the main thread)
- (void) setupThreading {
    if (_arr_ea_notifications) {
        return;
    }
    _arr_ea_notifications      = [[NSMutableArray alloc] init];
    _notificationLock   = [[NSLock alloc] init];
    _notificationThread = [NSThread currentThread];
    
    _notificationPort = [[NSMachPort alloc] init];
    [_notificationPort setDelegate:self];
    [[NSRunLoop currentRunLoop] addPort:_notificationPort
                                forMode:(NSString*)kCFRunLoopCommonModes];
}

// Should be called when CardCommunication thread is stopped
- (void) removeThreading    {
    _arr_ea_notifications = nil;
    _notificationLock = nil;
    _notificationThread = nil;

    [[NSRunLoop currentRunLoop] removePort:_notificationPort
                                forMode:(NSString*)kCFRunLoopCommonModes];
    _notificationPort = nil;
}

- (void) checkTimeOut   {
    if(!_thread_background || ![_thread_background isExecuting])  {
        logg(@"checkTimeOut - background thread is not active yet, no need to check for timeout");
        return;
    }
    [_lock_timeout_counter lock];
    if(_timeout_counter == -1)    {
        _timeout_counter = 0;
    }
    else    {
        _timeout_counter += 1;
    }
    logg(@"checkTimeOut - counter is %d", _timeout_counter);
    
    int timeout_limit = TIMEOUT_LIMIT;
    if(_reader.type == bai) {   // bai is very slow sometimes, give it more time before timeout
        timeout_limit *= 3;
    }
    if(_timeout_counter == timeout_limit)   {
        warn(@"Something must be hung!!!!!");
        [_timer_poll_status invalidate];    // Invalidate because it's repeating
        if(!_pthread_background)    {
            err(@"Failed to kill thread - pthread pointer not set");
            return;
        }
        int ret = pthread_kill(_pthread_background, SIGUSR1);
        if(ret == 0)    {
            warn(@"Killing hung thread - successful");
            [self resetReader];
            _timeout_counter = 0;
            // An accessory might be connected in the meanwhile, give it 3 seconds
            [self performSelector:@selector(tryDetectAlreadyConnectedAccessory) withObject:nil afterDelay:3];
        }
        else if(ret == EINVAL)   {
            err(@"Failed to kill thread - unsupported signal");
        }
        else if(ret == ESRCH)   {
            err(@"Failed to kill thread - invalid thread");
        }
        else    {
            err(@"Failed to kill thread -  return code %d", ret);
        }
    }
    [_lock_timeout_counter unlock];
}

/* 
 This signalHandler intercepts SIGUSR1 custom signal and kills the thread
 This is used to force kill a thread if it is suspected to hang
 Note that this is not ideal because we're using pthread_kill and pthread_exit methods to kill an
 NSThread object. An NSThread object is a complex wrapper for a pthread and killing through pthread methods
 prevent it from freeing memory gracefully, leaving uncollected memory garbage behind.
 For our purposes, [NSThread cancel] is not sufficient because we don't have control over SDK calls we make
 */
void signalHandler(int signal) {
    printf("\n\nForce kill thread\n\n");
    printf("\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
    pthread_exit(NULL);
}

#pragma mark - NSMachPort delegate

- (void) handleMachMessage:(void *)msg  {
    [_notificationLock lock];
    
    while ([_arr_ea_notifications count]) {
        NSNotification *notification = [_arr_ea_notifications objectAtIndex:0];
        [_arr_ea_notifications removeObjectAtIndex:0];
        [_notificationLock unlock];
        [self processNotification:notification];
        [_notificationLock lock];
    };
    
    [_notificationLock unlock];
}


#pragma mark - Properties
- (void) setReader_status:(long)new_reader_status   {
    long old_value = _reader_status;
    _reader_status = new_reader_status;
    if(new_reader_status != old_value) {
        
        if(new_reader_status != ID_READER_PROBLEM)    {
            // Reset error message if there is no error
            _reader_status_error_message = nil;
        }

        [self notifyStateChange];
    }
}

- (void) setCard_status:(long)new_card_status {
    long old_value = _card_status;
    _card_status = new_card_status;
    if(new_card_status != old_value) {
        
        if(new_card_status != ID_CARD_CONNECT_ERROR)    {
            // Reset error message if there is no error
            _card_status_error_message = nil;
        }
        
        [self notifyStateChange];
    }
}

#pragma mark - EAAccessory notifications


- (void) processNotification:(NSNotification *) notification    {
    if ([NSThread currentThread] != _thread_current) {
        // Forward the notification to the correct thread.
        logg(@"forwarding notification");
        [_notificationLock lock];
        [_arr_ea_notifications addObject:notification];
        [_notificationLock unlock];
        [_notificationPort sendBeforeDate:[NSDate date]
                                   components:nil
                                         from:nil
                                     reserved:0];
    }
    else {
        // Process the notification here
        if([notification.name isEqualToString:EAAccessoryDidConnectNotification]) {
            if(_thread_background && [_thread_background isExecuting])  {
                warn(@"A thread is already active, won't do anything");
                return;
            }
            
            [self setReader_status:ID_READER_PRESENT];
            
            // Check if accessory is supported
            EAAccessory *connectedAccessory = [[notification userInfo] objectForKey:EAAccessoryKey];
            
            [self resetReader];
            
            logg(@"examining protocols of connected accessory");
            for(NSString *protocol in connectedAccessory.protocolStrings)   {
                logg(@"protocol ||%@||", protocol);
                _reader = [self cardReaderFromAccessoryProtocol:protocol];
                if(_reader) {
                    logg(@"Supported card reader found");
                    break;
                }
            }
            if(!connectedAccessory.protocolStrings || [connectedAccessory.protocolStrings count] <= 0)    {
                [self setReader_status:ID_READER_TRYING];
                return;
            }
            
            if(!_reader)    {
                [self setReader_status:ID_READER_PROBLEM];
                [self setReader_status_error_message:@"Card reader not supported"];
            }
            else    {
                logg(@"Great, card reader supported");
                [self setReader_status:ID_READER_TRYING];
                
                // Start polling for card connection
                // (Although we do this check above, it's safe to do it here as well, just to avoid race conditions)
                if(_thread_background && [_thread_background isExecuting])  {
                    warn(@"A thread is already active, won't do anything 2");
                }
                else    {
                    logg(@"Starting a thread for pollCardStatus");
                    
                    _thread_background = [[NSThread alloc] initWithTarget:self selector:@selector(pollCardStatus) object:nil];
                    [_thread_background start];
                    
                }
            }
        }
        else if([notification.name isEqualToString:EAAccessoryDidDisconnectNotification])   {
            // TODO: If there is more than one accessory, this will fail
            // Replace this with AccessoryDelegate specific to used accessory
            
            [self setReader_status:ID_READER_ABSENT];
            [self setCard_status:ID_CARD_ABSENT];
            
            BOOL is_thread_running = NO;
            if(_thread_background && [_thread_background isExecuting])  {
                is_thread_running = YES;
                [_thread_background cancel];
            }
            
            if(is_thread_running)   {
                logg(@"will try to terminate thread");
                if(_thread_background && [_thread_background isExecuting])   {
                    [_thread_background cancel];
                }
                // Allow some time for thread to cancel
                usleep(500);
            }
            
            // Reset if thread stopped executing
            if(is_thread_running && ![_thread_background isExecuting])  {
                logg(@"thread terminated easily, will reset reader");
                [self resetReader];
            }
            else if(is_thread_running)   {
                logg(@"thread did NOT terminate easily, will rely on the timeout mechanism");
            }
            // else, in our force kill mechanism, the above will be called
        }
        
    }
}

-(void) accessoryDidConnect:(NSNotification *) notification {
    logg(@"NOTIFICATION - accessory connected");
    [self processNotification:notification];
}

-(void) accessoryDidDisconnect:(NSNotification *) notification {
    logg(@"NOTIFICATION - accessory disconnected");
    [self processNotification:notification];
}

#pragma mark - Background methods
- (void) pollCardStatus {
    @autoreleasepool {
        // Loop infinitely
        _timer_poll_status = [NSTimer scheduledTimerWithTimeInterval:CC_POLLING_PERIOD_S target:self selector:@selector(connectCard) userInfo:nil repeats:YES];
        
        // Set pthread reference
        _pthread_background = pthread_self();
        
        do  {
            [_lock_timeout_counter lock];
            _timeout_counter = 0;
            [_lock_timeout_counter unlock];
            
            // Start the run loop but return after each source is handled.
            SInt32    result = CFRunLoopRunInMode(kCFRunLoopDefaultMode, CC_POLLING_PERIOD_S, YES);
            
            [_lock_timeout_counter lock];
            _timeout_counter = -1;
            [_lock_timeout_counter unlock];
            
            // If a source explicitly stopped the run loop, or if there are no
            // sources or timers, go ahead and exit.
            if(result == kCFRunLoopRunStopped)  {
                warn(@"Background thread stopped");
            }
            else if (result == kCFRunLoopRunFinished)  {
                warn(@"Background thread runloop finished");
                break;
            }
            else if([[NSThread currentThread] isCancelled]) {
                warn(@"Background thread cancelled");
                break;
            }            
        }
        while (true);
        [_timer_poll_status invalidate];

        logg(@"pollCardStatus finishing");
    }
}

// Calls connectCard on the reader and sets card and reader status accordingly
- (void) connectCard    {
    long resp = [_reader connectCard];
    logg(@"connectCard response %@", [CardCommunication responseString:resp]);
    if([[NSThread currentThread] isCancelled])   {
        return;
    }
       
    switch (resp) {
        case ID_OK:
            warn(@"Don't know how to respond to OK");
            break;
        case ID_READER_ACCESS_SUCCESSFUL:
            [self setReader_status:ID_READER_ACCESS_SUCCESSFUL];
            break;
        case ID_CARD_ACCESS_SUCCESSFUL:
            [self setCard_status:ID_CARD_ACCESS_SUCCESSFUL];
            [self setReader_status:ID_READER_ACCESS_SUCCESSFUL];
            break;
        case ID_CARD_CONNECT_ERROR:
            [self setReader_status:ID_READER_ACCESS_SUCCESSFUL];
            [self setCard_status:ID_CARD_CONNECT_ERROR];
            break;
        case ID_CARD_ABSENT:
            [self setReader_status:ID_READER_ACCESS_SUCCESSFUL];
            [self setCard_status:ID_CARD_ABSENT];
            break;
        case ID_READER_PROBLEM:
            [self setReader_status:ID_READER_PROBLEM];
            [self setCard_status:ID_CARD_CONNECT_ERROR];
            break;
        case ID_READER_ABSENT:
            [self setReader_status:ID_READER_ABSENT];
            [self setCard_status:ID_CARD_CONNECT_ERROR];
            break;
        case ID_READER_CONTEXT_ERROR:
            [self setReader_status:ID_READER_CONTEXT_ERROR];
            [self setReader_status_error_message:@"Failed to establish context"];
            [self setCard_status:ID_CARD_CONNECT_ERROR];
            break;
        case ID_ERROR:
            [self setReader_status:ID_READER_PROBLEM];
            [self setCard_status:ID_CARD_CONNECT_ERROR];
            break;
        case ID_READER_TRYING:
            [self setReader_status:ID_READER_TRYING];
            break;
        default:
            warn(@"Unrecognised response from connectCard");
            break;
    }
    
    [_lock_timeout_counter lock];
    _timeout_counter = -1;
    [_lock_timeout_counter unlock];    
}

#pragma mark - Convenience methods

- (void) tryDetectAlreadyConnectedAccessory {
    if(![NSThread isMainThread])    {
        // Some readers (e.g. Bai) expect accessory detection to take place on the main thread
        [self performSelectorOnMainThread:@selector(tryDetectAlreadyConnectedAccessory) withObject:nil waitUntilDone:NO];
        return;
    }
    EAAccessoryManager *manager = [EAAccessoryManager sharedAccessoryManager];
    for(EAAccessory *acc in [manager connectedAccessories])    {
        logg(@"generating a fake EA notification for already connected accessory");
        // Fake generate a ConnectedAccessory notification
        NSDictionary *dict_info = [NSDictionary dictionaryWithObject:acc forKey:EAAccessoryKey];
        [[NSNotificationCenter defaultCenter] postNotificationName:EAAccessoryDidConnectNotification object:nil userInfo:dict_info];
    }
}

// NOTE: This method should be called in the same thread where reader was configured
// (Config creates context which can only be destroyed if called from the same thread)
- (void) resetReader    {
    logg(@"+ resetReader");
    [self setReader_status:ID_READER_ABSENT];
    if(_reader) {
        [_reader reset];
        [self setReader:nil];
        
        // Ensure you are still registered for EAAccessory callbacks
        // (might get de-registered during reset of SDK's)
        
        // Register for accessory notifications (first un-register, to be on the safe side. Otherwise, same notification is received multiple times)
        [[EAAccessoryManager sharedAccessoryManager] unregisterForLocalNotifications];
        [[NSNotificationCenter defaultCenter] removeObserver:self];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(accessoryDidConnect:) name:EAAccessoryDidConnectNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(accessoryDidDisconnect:) name:EAAccessoryDidDisconnectNotification object:nil];
        
        [[EAAccessoryManager sharedAccessoryManager] registerForLocalNotifications];
    }
}

// Called to post status change notification, whenever there is a status change for card or reader
// Ensure that card_status, reader_status, card_status_message and reader_status_message properties are
// set properly before calling this method
- (void) notifyStateChange  {
    logg(@"+ notifyStateChange - %@ %@", [CardCommunication readerStatusString:_reader_status] , [CardCommunication cardStatusString:_card_status]);
    
    if([NSThread currentThread] != _thread_current) {
        logg(@"Forwarding notifyStateChange to current thread");
        [self performSelector:@selector(notifyStateChange) onThread:_thread_current withObject:nil waitUntilDone:NO];
        return;
    }
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithCapacity:4];
    [dict setObject:[NSNumber numberWithInt:_card_status] forKey:@"card_status"];
    [dict setObject:[NSNumber numberWithInt:_reader_status] forKey:@"reader_status"];
    if(_card_status_error_message)  {
        [dict setObject:_card_status_error_message forKey:@"card_status_error_message"];
    }
    if(_reader_status_error_message)    {
        [dict setObject:_reader_status_error_message forKey:@"reader_status_error_message"];
    }

    
    if(!_is_pooled_notification_mode)    {
        [[NSNotificationCenter defaultCenter]
         postNotificationName:CCNOTIFICATION_STATUS_CHANGE object:nil userInfo:dict];
    }
    else    {
        // Invalidate existing notifications
        // (Only send the latest one)
        // NOTE: Beware there is a risk of never sending a notification here (however, if connectCard works properly, then this should never happen. Risk occurs only when the state is constantly changing)

        for(NSTimer *timer in _arr_timed_notifications)   {
            if(timer)   {
                [timer invalidate];
            }
        }
        [_arr_timed_notifications removeAllObjects];

        float time = CC_NOTIFICATION_TIME_WINDOW_DEFAULT;
        if([_reader respondsToSelector:@selector(CC_NOTIFICATION_TIME_WINDOW)])  {
            time = [_reader CC_NOTIFICATION_TIME_WINDOW];
        }
        
        NSTimer *timed_notification = [NSTimer scheduledTimerWithTimeInterval:time target:self selector:@selector(fireNotification:) userInfo:dict repeats:NO];
        [_arr_timed_notifications addObject:timed_notification];
    }
}

- (void) fireNotification:(NSTimer *) timer {
    if([timer isValid]) {
        logg(@"firing notification");
        [[NSNotificationCenter defaultCenter]
         postNotificationName:CCNOTIFICATION_STATUS_CHANGE object:nil userInfo:[timer userInfo]];
    }
    else    {
        logg(@"Notification cancelled");
    }
    timer = nil;
}

- (CardReader *) cardReaderFromAccessoryProtocol:(NSString *) protocol    {
    // Returns CardReader object to handle given External Accessory protocol. nil if protocol is not supported
    protocol = [[Helper string_trimmed:protocol] lowercaseString];
    
    NSString *class_name = nil;
    if([protocol isEqualToString:@"com.ewcdma.i-dentity"] )  {
        class_name = @"CardReader_identity";
    }
    else if([protocol isEqualToString:@"com.ewcdma.ecarder"]) {
//        class_name = @"CardReader_kimallstar";
        // identity library is added, hence we will use the identity reader for kimallstar
        class_name = @"CardReader_identity";
    }
    else if([protocol isEqualToString:@"com.precisebiometrics.ccidcontrol"] ||
            [protocol isEqualToString:@"com.precisebiometrics.ccidinterrupt"] ||
            [protocol isEqualToString:@"com.precisebiometrics.ccidbulk"] ||
            [protocol isEqualToString:@"com.precisebiometrics.sensor"])  {
        class_name = @"CardReader_precise";
    }
    else if([protocol isEqualToString:@"com.idtechproducts.ismart"])    {
        class_name = @"CardReader_idtech";
    }
    else if([protocol isEqualToString:@"com.baimobile.reader"])    {
        class_name = @"CardReader_bai";
    }
    else if([protocol isEqualToString:@"com.ftsafe.crd"] ||
            [protocol isEqualToString:@"com.ftsafe.ir301"])    {
        class_name = @"CardReader_feitian";
    }
    else    {
        warn(@"Unrecognised protocol : %@" , protocol);
    }

    
    if(class_name)  {
        CardReader *reader_object = [[NSClassFromString(class_name) alloc] init];
        [reader_object config];
        return reader_object;
    }
    
    return nil;
}

#pragma mark - Enum to string conversion methods
+ (NSString *) cardStatusString:(long)status  {
    switch (status) {
        case ID_CARD_CONNECT_ERROR:
            return @"card_access_error";
        case ID_CARD_ACCESS_SUCCESSFUL:
            return @"card_access_success";
        case ID_CARD_PRESENT:
            return @"card_detected";
        case ID_CARD_ABSENT:
            return @"card_not_found";
        default:
            return @"(unrecognised code)";
    }
}

+ (NSString *) readerStatusString:(long)status  {
    switch (status) {
        case ID_READER_PROBLEM:
            return @"reader_access_error";
        case ID_READER_ACCESS_SUCCESSFUL:
            return @"reader_access_success";
        case ID_READER_PRESENT:
            return @"reader_detected";
        case ID_READER_ABSENT:
            return @"reader_not_found";
        case ID_READER_TRYING:
            return @"reader_access_trying";
        default:
            return @"(unrecognised code)";
    }
}

+ (NSString *) responseString:(long)response {
    switch (response) {
        case ID_ASK_MESSAGE:
            return @"ID_ASK_MESSAGE";
        case ID_CARD_ABSENT:
            return @"ID_CARD_ABSENT";
        case ID_CARD_ACCESS_SUCCESSFUL:
            return @"ID_CARD_ACCESS_SUCCESSFUL";
        case ID_CARD_CONNECT_ERROR:
            return @"ID_CARD_CONNECT_ERROR";
        case ID_CARD_GET_ADDRESS_ERROR:
            return @"ID_CARD_GET_ADDRESS_ERROR";
        case ID_CARD_GET_CA_CERTIFICATE_ERROR:
            return @"ID_CARD_GET_CA_CERTIFICATE_ERROR";
        case ID_CARD_GET_CERTIFICATE_ERROR:
            return @"ID_CARD_GET_CERTIFICATE_ERROR";
        case ID_CARD_GET_IDENTITY_ERROR:
            return @"ID_CARD_GET_IDENTITY_ERROR";
        case ID_CARD_GET_PHOTO_ERROR:
            return @"ID_CARD_GET_PHOTO_ERROR";
        case ID_CARD_GET_SIGN_DATA_ERROR:
            return @"ID_CARD_GET_SIGN_DATA_ERROR";
        case ID_CARD_INVALID_ATR:
            return @"ID_CARD_INVALID_ATR";
        case ID_CARD_KEY_SELECTION_ERROR:
            return @"ID_CARD_KEY_SELECTION_ERROR";
        case ID_CARD_NOT_VALID:
            return @"ID_CARD_NOT_VALID";
        case ID_CARD_OPERATION_ERROR:
            return @"ID_CARD_OPERATION_ERROR";
        case ID_CARD_PRESENT:
            return @"ID_CARD_PRESENT";
        case ID_CARD_SIGNATURE_ERROR:
            return @"ID_CARD_SIGNATURE_ERROR";
        case ID_CARD_TRYING:
            return @"ID_CARD_TRYING";
        case ID_CARD_VERIFY_PIN_ERROR:
            return @"ID_CARD_VERIFY_PIN_ERROR";
        case ID_ERROR:
            return @"ID_ERROR";
        case ID_ERROR_MESSAGE:
            return @"ID_ERROR_MESSAGE";
        case ID_INVALID_LICENSE:
            return @"ID_INVALID_LICENSE";
        case ID_MESSAGE:
            return @"ID_MESSAGE";
        case ID_MOVE_PROGRESS:
            return @"ID_MOVE_PROGRESS";
        case ID_OK:
            return @"ID_OK";
        case ID_READER_ABSENT:
            return @"ID_READER_ABSENT";
        case ID_READER_ACCESS_SUCCESSFUL:
            return @"ID_READER_ACCESS_SUCCESSFUL";
        case ID_READER_CONTEXT_ERROR:
            return @"ID_READER_CONTEXT_ERROR";
        case ID_READER_NOT_VALID:
            return @"ID_READER_NOT_VALID";
        case ID_READER_NOT_VALID_STATUS:
            return @"ID_READER_NOT_VALID_STATUS";
        case ID_READER_PRESENT:
            return @"ID_READER_PRESENT";
        case ID_READER_PROBLEM:
            return @"ID_READER_PROBLEM";
        case ID_READER_TRYING:
            return @"ID_READER_TRYING";
        case ID_START_PROGRESS:
            return @"ID_START_PROGRESS";
        case ID_STOP_PROGRESS:
            return @"ID_STOP_PROGRESS";            
            
        default:
            return @"(unrecognised cc_response)";
    }
}

@end
