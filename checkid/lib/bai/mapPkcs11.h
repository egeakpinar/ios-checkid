/*
 *  mapPkcs11.h
 *  
 *  Created by Mike Pirnat on 9/28/11.
 *  Copyright 2010 Biometric Associates, LP. All rights reserved.
 *
 *  derived from the RSA Security Inc. PKCS #11 Cryptographic Token Interface (Cryptoki) 
 */

// Include this header file to use normal Pkcs11 names

#pragma once

#define C_Initialize(a) BAL_C_Initialize(a)
#define C_Finalize(a) BAL_C_Finalize(a)
#define C_GetInfo(a) BAL_C_GetInfo(a)
#define C_GetSlotList(a,b,c) BAL_C_GetSlotList(a,b,c)
#define C_OpenSession(a,b,c,d,e) BAL_C_OpenSession(a,b,c,d,e)
#define C_CloseSession(a) BAL_C_CloseSession(a)
#define C_Login(a,b,c,d) BAL_C_Login(a,b,c,d)
#define C_Logout(a) BAL_C_Logout(a)
#define C_CreateObject(a,b,c,d) BAL_C_CreateObject(a,b,c,d)
#define C_DestroyObject(a,b) BAL_C_DestroyObject(a,b)
#define C_GetAttributeValue(a,b,c,d) BAL_C_GetAttributeValue(a,b,c,d)
#define C_FindObjectsInit(a,b,c) BAL_C_FindObjectsInit(a,b,c)
#define C_FindObjects(a,b,c,d) BAL_C_FindObjects(a,b,c,d)
#define C_FindObjectsFinal(a) BAL_C_FindObjectsFinal(a)
#define C_EncryptInit(a,b,c) BAL_C_EncryptInit(a,b,c)
#define C_Encrypt(a,b,c,d,e) BAL_C_Encrypt(a,b,c,d,e)
#define C_DecryptInit(a,b,c) BAL_C_DecryptInit(a,b,c)
#define C_Decrypt(a,b,c,d,e) BAL_C_Decrypt(a,b,c,d,e)
#define C_DigestInit(a,b) BAL_C_DigestInit(a,b)
#define C_Digest(a,b,c,d,e) BAL_C_Digest(a,b,c,d,e)
#define C_SignInit(a,b,c) BAL_C_SignInit(a,b,c)
#define C_Sign(a,b,c,d,e) BAL_C_Sign(a,b,c,d,e)
#define C_VerifyInit(a,b,c) BAL_C_VerifyInit(a,b,c)
#define C_Verify(a,b,c,d,e) BAL_C_Verify(a,b,c,d,e)
#define C_GenerateKey(a,b,c,d,e) BAL_C_GenerateKey(a,b,c,d,e)
#define C_SeedRandom(a,b,c) BAL_C_SeedRandom(a,b,c)
#define C_GenerateRandom(a,b,c) BAL_C_GenerateRandom(a,b,c)