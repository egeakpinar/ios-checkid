/*
 *  BAIModuleConnect.h
 *  baimodules-ios-sdk
 *
 *  Copyright 2011 Biometric Associates. All rights reserved.
 *
 */

#ifndef __BAIINTERFACE_IOS_CONNECT_H__
#define __BAIINTERFACE_IOS_CONNECT_H__

#include <Foundation/Foundation.h>
#include <UIKit/UIKit.h>
#include "BAIInterface.h"
#include "BAI-defines-public.h"
#include "bai-pcsc-controlcodes.h"

#define BAI_CONNECTION_VIEW_OPTION_GET_PIN				0x01
#define BAI_CONNECTION_VIEW_OPTION_CONNECT				0x02
#define BAI_CONNECTION_VIEW_OPTION_GET_PIN_AND_CONNECT	0x03
#define BAI_CONNECTION_VIEW_OPTION_PIN_BLOCKED			0x04

#ifdef __cplusplus
extern "C" {
#endif


/**
 *
 * Displays the connection view controller and blocks until
 * the view controller has been removed.
 *
 * PARAMETERS:
 *	context:	An established context. The context must remain
 *				established while a thread is calling into this
 *				function.
 *	controller:	A UINavigationController that is used to push
 *				the connection view controller.
 *	options:	A bit mask of the connection options. 
 *				If PIN_BLOCKED option is used then no
 *				pin collection or connection attempt are made.
 *	pin:		A pointer to the storage location for the collected
 *				pin or NULL if no pin collection is needed.
 *			
 * RETURN VALUE: 
 *	BAIRESPONSE_SUCCESS : Success
 *	BAIRESPONSE_CANCELLED_BY_USER : User dismissed the UI
 *	Or any other error code that is listed.
 *	
 * USING A CACHED PIN: If the pin object lenth if > 0 when
 *	the connection screen is displayed then the pin field
 *	on the screen will show a string of astricks. The user 
 *	will be able to clear or modify the pin.
 *
 * THREADING: This function must be called from a non-main
 *	thread. The function will block and not return until it
 *	returns success or the view has been dismissed.
 *
 * RESOURCES: This function requires that the NSBundle with
 *	identifer com.baimobile.resources is present within the
 *	Application. 
 *
 * NOTE: Using options mask 0x01 is not supported. 
 *		Supported masks are : 0x02, 0x03, 0x04
 *
 */

BAIRESPONSE bai_interface_show_connection_view(
													  hBAIContext hContext,
													  UINavigationController* controller, 
													  Byte options, 
													  NSMutableData* pin);
	
BAIRESPONSE bai_interface_set_navigation_controller(
														   UINavigationController* controller,
														   id obj
														   );
	
unsigned int bai_interface_connect_module_version();



#ifdef __cplusplus
}
#endif

#endif // __BAIINTERFACE_IOS_CONNECT_H__
