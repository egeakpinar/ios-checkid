/*
 * MUSCLE SmartCard Development ( http://www.linuxnet.com )
 *
 * Copyright (C) 1999-2003
 *  David Corcoran <corcoran@linuxnet.com>
 * Copyright (C) 2002-2009
 *  Ludovic Rousseau <ludovic.rousseau@free.fr>
 *
 * $Id: winscard.h 5341 2010-10-22 11:36:06Z rousseau $
 */

/**
 * @file
 * @brief This handles smartcard reader communications.
 */

#ifndef __winscard_h__
#define __winscard_h__


#include "wintypes.h"

#ifdef __cplusplus
extern "C"
{
#endif

#ifndef PCSC_API
#define PCSC_API
#endif
    
#define MAX_ATR_SIZE			33
	
    typedef LONG SCa04CONTEXT; /**< \p hContext returned by SCardEstablishContext() */
    typedef SCARDCONTEXT *PSCARDCONTEXT;
    typedef SCARDCONTEXT *LPSCARDCONTEXT;
    typedef LONG SCa04HANDLE; /**< \p hCard returned by SCardConnect() */
    typedef SCARDHANDLE *PSCARDHANDLE;
    typedef SCARDHANDLE *LPSCARDHANDLE;
    
    
    typedef struct
    {
        const char *szReader;
        void *pvUserData;
        DWORD dwCurrentState;
        DWORD dwEventState;
        DWORD cbAtr;
        unsigned char rgbAtr[MAX_ATR_SIZE];
    }
    SCARD_READERSTATE, *LPSCARD_READERSTATE;
	/** Protocol Control Information (PCI) */
	typedef struct _SCARD_IO_REQUEST
	{
		uint32_t dwProtocol;	/**< Protocol identifier */
		uint32_t cbPciLength;	/**< Protocol Control Inf Length */
	}
	SCARD_IO_REQUEST, *PSCARD_IO_REQUEST, *LPSCARD_IO_REQUEST;
	
	typedef const SCARD_IO_REQUEST *LPCSCARD_IO_REQUEST;
	
	extern SCARD_IO_REQUEST g_rgSCardT0Pci, g_rgSCardT1Pci,
	g_rgSCardRawPci;

	PCSC_API LONG SCa04EstablishContext(DWORD dwScope,
		/*@null@*/ LPCVOID pvReserved1, /*@null@*/ LPCVOID pvReserved2,
		/*@out@*/ LPSCARDCONTEXT phContext);

	PCSC_API LONG SCa04ReleaseContext(SCARDCONTEXT hContext);

	PCSC_API LONG SCa04IsValidContext(SCARDCONTEXT hContext);

	PCSC_API LONG SCa04Connect(SCARDCONTEXT hContext,
		LPCSTR szReader,
		DWORD dwShareMode,
		DWORD dwPreferredProtocols,
		/*@out@*/ LPSCARDHANDLE phCard, /*@out@*/ LPDWORD pdwActiveProtocol);

	PCSC_API LONG SCa04Reconnect(SCARDHANDLE hCard,
		DWORD dwShareMode,
		DWORD dwPreferredProtocols,
		DWORD dwInitialization, /*@out@*/ LPDWORD pdwActiveProtocol);

	PCSC_API LONG SCa04Disconnect(SCARDHANDLE hCard, DWORD dwDisposition);

	PCSC_API LONG SCa04BeginTransaction(SCARDHANDLE hCard);

	PCSC_API LONG SCa04EndTransaction(SCARDHANDLE hCard, DWORD dwDisposition);

	PCSC_API LONG SCa04Status(SCARDHANDLE hCard,
		/*@null@*/ /*@out@*/ LPSTR mszReaderName,
		/*@null@*/ /*@out@*/ LPDWORD pcchReaderLen,
		/*@null@*/ /*@out@*/ LPDWORD pdwState,
		/*@null@*/ /*@out@*/ LPDWORD pdwProtocol,
		/*@null@*/ /*@out@*/ LPBYTE pbAtr,
		/*@null@*/ /*@out@*/ LPDWORD pcbAtrLen);

	PCSC_API LONG SCa04GetStatusChange(SCARDCONTEXT hContext,
		DWORD dwTimeout,
		LPSCARD_READERSTATE rgReaderStates, DWORD cReaders);

	PCSC_API LONG SCa04Control(SCARDHANDLE hCard, DWORD dwControlCode,
		LPCVOID pbSendBuffer, DWORD cbSendLength,
		/*@out@*/ LPVOID pbRecvBuffer, DWORD cbRecvLength,
		LPDWORD lpBytesReturned);

	PCSC_API LONG SCa04Transmit(SCARDHANDLE hCard,
		const SCARD_IO_REQUEST *pioSendPci,
		LPCBYTE pbSendBuffer, DWORD cbSendLength,
		/*@out@*/ SCARD_IO_REQUEST *pioRecvPci,
		/*@out@*/ LPBYTE pbRecvBuffer, LPDWORD pcbRecvLength);

	PCSC_API LONG SCa04ListReaderGroups(SCARDCONTEXT hContext,
		/*@out@*/ LPSTR mszGroups, LPDWORD pcchGroups);

	PCSC_API LONG SCa04ListReaders(SCARDCONTEXT hContext,
		/*@null@*/ /*@out@*/ LPCSTR mszGroups,
		/*@null@*/ /*@out@*/ LPSTR mszReaders,
		/*@out@*/ LPDWORD pcchReaders);

	PCSC_API LONG SCa04Cancel(SCARDCONTEXT hContext);
    
    PCSC_API  LONG SCa04SetTimeout(SCARDCONTEXT hContext, DWORD dwTimeout);

	PCSC_API LONG SCa04GetAttrib(SCARDHANDLE hCard, DWORD dwAttrId,
		/*@out@*/ LPBYTE pbAttr, LPDWORD pcbAttrLen);
    
    LONG FtGetSerialNum(unsigned int reader_index, unsigned int  length,
                                      char * buffer);
    LONG FtWriteFlash(unsigned int reader_index,unsigned char bOffset, unsigned char blength,
                      unsigned char buffer[]);
    LONG FtReadFlash(unsigned int reader_index,unsigned char bOffset, unsigned char blength,
                     unsigned char buffer[]);
    
#ifdef __cplusplus
}
#endif

#endif

