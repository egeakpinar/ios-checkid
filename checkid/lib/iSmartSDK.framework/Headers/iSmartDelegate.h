//
//  iSmartDelegate.h
//  iSmartSDK
//
//  Created by Chris Powell on 11/11/11.
//  Copyright (c) 2011 CHARGE Anywhere LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@class iSmart;

@protocol iSmartDelegate <NSObject>

-(void) iSmartDidConnect;
-(void) iSmartDidDisconnect;
-(void) cardStatusChanged:(unsigned char)status;

@end
