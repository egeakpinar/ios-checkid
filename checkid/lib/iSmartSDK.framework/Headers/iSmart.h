//
//  iSmart.h
//  iSmartSDK
//
//  Created by Chris Powell on 11/3/11.
//  Copyright (c) 2011 CHARGE Anywhere LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ExternalAccessory/ExternalAccessory.h>

typedef enum{
  LED_CONTROL_TURN_OFF=0x30,
  LED_CONTROL_TURN_ON_GREEN,
  LED_CONTROL_TURN_ON_RED,
  LED_CONTROL_TURN_ON_AMBER,
  LED_CONTROL_FLASH_RED_AND_AMBER,
  LED_CONTROL_FLASH_GREEN,
  LED_CONTROL_FLASH_RED,
  LED_CONTROL_FLASH_AMBER
} LEDControlCharacter;

typedef enum{
  OPERATION_MODE_FLAG_CARD_SEATED_CHANGED=2,
  OPERATION_MODE_FLAG_LED_CONTROL=16
}OperationModeFlag;

typedef enum{
  POWER_ON_OPTION_IFS_FLAG=1,
  POWER_ON_OPTION_EXPLICIT_PPS_FLAG=2,
  POWER_ON_OPTION_AUTO_PPS_FLAG=64,
  POWER_ON_OPTION_IFS_RESPONSE_CHECK_FLAG=128
}PowerOnOption;

typedef enum{
  MEMORY_CARD_TYPE_3_BYTE_I2C=0,
  MEMORY_CARD_TYPE_4_BYTE_I2C,
  MEMORY_CARD_TYPE_SLE44x8,
  MEMORY_CARD_TYPE_SLE44x2,
  MEMORY_CARD_TYPE_GPM276,
  MEMORY_CARD_TYPE_GPM271,
  MEMORY_CARD_TYPE_SLE4404_AT88SC101,
  MEMORY_CARD_TYPE_SLE4406
}MemoryCardType;

@class ISO7816Command;
@class T0Command;
@protocol iSmartDelegate;

@interface iSmart : NSObject<EAAccessoryDelegate>{
  id<iSmartDelegate> delegate;
}

@property(nonatomic, assign) id<iSmartDelegate> delegate;

-(id)init;
-(BOOL)open;
-(void)close;

-(NSString*) firmwareVersion;
-(unsigned char) readerStatus;
-(NSData*) readerSettings;
-(void) sendLEDControlCharacter:(LEDControlCharacter)controlCharacter;
-(T0Command*)resetReader;
-(NSString*) copyRightInformation;
-(T0Command*) restoreDefaultReaderSettings;
-(T0Command*) setOperationMode:(unsigned char)operationMode;
-(T0Command*) setCardOption:(unsigned char) cardOption;
-(ISO7816Command*) powerOnCardWithOptions:(NSData*)optionFlagsOrNil PPSBytes:(NSData*)ppsBytesOrNil;
-(ISO7816Command*) powerOffCard;
-(ISO7816Command*) sendCommandToCPUCard:(ISO7816Command*)command;
-(ISO7816Command*) sendCommandToMemoryCard:(ISO7816Command*)command;
-(T0Command*) setMemoryCardType:(MemoryCardType)memoryCardType;
-(T0Command*) setMemoryCardWriteSize:(unsigned char)memoryCardWriteSize;
-(NSData*) directIO:(NSData*)rawBytes;

@end
