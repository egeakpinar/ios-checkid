//
//  CardCommunication.h
//  checkid
//
//  Created by Ismail Ege AKPINAR on 14/03/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IDCodes.h"
#import "CardReader.h"
#import "Card.h"

// This class posts the following notification(s) via NotificationCenter
#define CCNOTIFICATION_STATUS_CHANGE         @"CCNOTIFICATION_01"
#define CC_NOTIFICATION_TIME_WINDOW_DEFAULT     1.0f    // Allow x s before a notification is sent (used to pool notifications in case there are many and only sends the most recent one)
// Note that NOTIFICATION_TIME_WINDOW may be configured separately for each card_reader by implementing CC_NOTIFICATION_TIME_WINDOW method
#define CC_POLLING_PERIOD_S   1.0f    // Poll card status every x seconds
#define TIMEOUT_LIMIT 3 // Kill a thread after 3 subsequent timeout warnings

@interface CardCommunication : NSObject<NSMachPortDelegate>

@property (nonatomic, assign) BOOL is_pooled_notification_mode; // YES (default) if only last notification within CC_NOTIFICATION_TIME_WINDOW should be sent, NO if all notifications should be sent as they occur
@property (nonatomic, assign) BOOL is_timeout_enabled;  // YES (default) if a timeout mechanism is in place to force kill hung threads (that occur during communication with reader SDK's), NO if this mechanism is disabled
@property (nonatomic, retain) CardReader *reader;

// Current connection status
@property (nonatomic, assign) long reader_status;
@property (nonatomic, retain) NSString *reader_status_error_message;    // Valid only when there is error
@property (nonatomic, assign) long card_status;
@property (nonatomic, retain) NSString *card_status_error_message;      // Valid only when there is error

// DEBB
@property (nonatomic, retain) NSThread *thread_background;  // Reference to thread used by SDK's

// Singleton instance
+ (CardCommunication *) instance;

- (void) start;
- (void) stop;

// Enum to string conversion methods
+ (NSString *) cardStatusString:(long)status;
+ (NSString *) readerStatusString:(long)status;
+ (NSString *) responseString:(long)response;
@end
