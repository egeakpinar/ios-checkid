//
//  Cell_Switch.m
//  checkid
//
//  Created by Ismail Ege AKPINAR on 04/03/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import "Cell_Switch.h"
#import "Debug.h"

@implementation Cell_Switch

#pragma mark - Default init methods
- (void) awakeFromNib   {
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    // Configure autoresizing masks
    [_switchOnOff setAutoresizingMask:(UIViewAutoresizingFlexibleLeftMargin)];
}

- (IBAction)switchValueChanged:(id)sender {
    if(_delegate && [_delegate conformsToProtocol:@protocol(Cell_Switch_Delegate)])   {
        [_delegate cellSwitch:self valueChanged:sender];
    }
}
@end
