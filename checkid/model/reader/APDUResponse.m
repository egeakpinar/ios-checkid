//
//  APDUResponse.m
//  checkid
//
//  Created by Ismail Ege AKPINAR on 28/03/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import "APDUResponse.h"
#import "Debug.h"

@implementation APDUResponse

-(BOOL)isOK {
    return self.sw1 == 0x90 && self.sw2 == 0x00;
}

- (void) log    {
    logg(@"APDUResponse data ||%@|| , sw1 ||0x%x|| , sw2 ||0x%x||, isOK %d",_data,_sw1,_sw2,[self isOK]);
}
@end
