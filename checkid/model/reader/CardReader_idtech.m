//
//  CardReader_idtech.m
//  checkid
//
//  Created by Ismail Ege AKPINAR on 14/03/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import "CardReader_idtech.h"
#import "Debug.h"
#import "CCCommon.h"

#import <iSmartSDK/iSmart.h>
#import <iSmartSDK/T0Command.h>
#import <iSmartSDK/T1Command.h>

@interface CardReader_idtech()
    @property(nonatomic, retain) iSmart *device;
@end

@implementation CardReader_idtech

#pragma mark - CardReader properties

- (NSData *)getATR  {
    warn(@"getATR is not supported for idtech reader");
    return nil;
}

#pragma mark - CardReader methods

- (void) config {
    _device = [[iSmart alloc] init];
    [_device setDelegate:self];
    is_opened = NO;
}

- (void) reset  {
    [_device close];
    _device = nil;
    is_opened = NO;
}

/*- (cc_response) sendCommand:(NSString *)command {
    T0Command *t0Command = [[T0Command alloc] init];
    ISO7816Command *resultCommand = nil;
    
    t0Command.classByte = 0;
    t0Command.instructionByte = 0x20;
    t0Command.parameter1Byte = 0;
    t0Command.parameter2Byte = 0x01;
    t0Command.lengthByte = 0x08;
    
    NSString *PIN = @"241234FFFFFFFFFF";
    const char *buf = [PIN UTF8String];
    NSMutableData *data = [NSMutableData data];
    if (buf)
    {
        uint32_t len = strlen(buf);
        
        char singleNumberString[3] = {'\0', '\0', '\0'};
        uint32_t singleNumber = 0;
        for(uint32_t i = 0 ; i < len; i+=2)
        {
            if ( ((i+1) < len) && isxdigit(buf[i]) && (isxdigit(buf[i+1])) )
            {
                singleNumberString[0] = buf[i];
                singleNumberString[1] = buf[i + 1];
                sscanf(singleNumberString, "%x", &singleNumber);
                uint8_t tmp = (uint8_t)(singleNumber & 0x000000FF);
                [data appendBytes:(void *)(&tmp) length:1];
            }
            else
            {
                break;
            }
        }
        t0Command.dataBytes = data;
        NSLog(@"data length %d", [data length]);
    }
    
    t0Command.commandId = WRITE_CPU_CARD_COMMAND_ID;
    resultCommand = [_device sendCommandToCPUCard:t0Command];
    
    if (resultCommand == nil) {
        err(@"Verify PIN failed");
        return cc_response_Error;
    } else  {
        logg(@"Verify PIN status: %02x%02x", ((T0Command*)resultCommand).status1Byte, ((T0Command*)resultCommand).status2Byte);
        return cc_response_OK;
    }    
}*/

/*- (cc_response) sendCommand:(NSString *)command {
    NSData *data_send = [CCCommon stringToData:command];
    if(data_send == nil)    {
        err(@"Could not create command bytes");
        return cc_response_Error;
	}

    
    NSData *data_return = [_device directIO:data_send];
    
    logg(@"return %@", data_return);
    return cc_response_OK;
}*/

- (APDUResponse *) sendCommand:(NSString *)command {
    APDUResponse *response = [[APDUResponse alloc] init];
    
    // TODO: It's harcoded to use PCI_PROTOCOL_0 and WRITE_CPU_CARD_COMMAND_ID
    T0Command *t0Command = (T0Command *)[CardReader_idtech ISO7816CommandFromString:command protocol:PCI_PROTOCOL_0 commandId:WRITE_CPU_CARD_COMMAND_ID];

    if(t0Command == nil)    {
        err(@"Failed to create command from string ||%@||", command);
        return nil;
    }
    
    ISO7816Command *result_command = [_device sendCommandToCPUCard:t0Command];
    if (result_command == nil) {
        err(@"Verify PIN failed");
        return nil;
    } else  {
        logg(@"Verify PIN status: %02x%02x", ((T0Command*)result_command).status1Byte, ((T0Command*)result_command).status2Byte);
        
        T0Command* result_command_t0 = (T0Command *)result_command;
        response.sw1 = result_command_t0.status1Byte;
        response.sw2 = result_command_t0.status2Byte;
        response.data = result_command_t0.dataBytes;
    }
    return response;
    
}


static BOOL is_opened = NO;
- (long) connectCard {
    // TODO: Protocol is hardcoded to be 0
    self.active_protocol = PCI_PROTOCOL_0;
    if(!is_opened)   {
        // [device open] should be called only once
        if([_device open]) {
            logg(@"open successful");
            is_opened = YES;
        }
        else    {
            logg(@"open failed");
            is_opened = NO;
            return ID_READER_CONTEXT_ERROR;
        }
    }
    
    unsigned char stat = [_device readerStatus];
    
    logg(@"stat 0x%02x", stat);
    
    if( (stat & 2) == 2)    {
        logg(@"Card OK");
        return ID_CARD_ACCESS_SUCCESSFUL;
    }
    else    {
        logg(@"no card");
        
        if( (stat & 1) == 1)    {
            logg(@"power on");
            return ID_CARD_CONNECT_ERROR;
        }
        else    {
            logg(@"no power");
        }

        
        return ID_CARD_ABSENT;
    }
}

- (long) powerOnCard {
 
    // Must already be opened
    if(!is_opened)  {
        warn(@"Cannot power on Card, it is not opened yet");
        return ID_READER_CONTEXT_ERROR;
    }

    // Note: This command hangs if executed at the same time reader is ejected
    T0Command *resultCommand = (T0Command*)[_device powerOnCardWithOptions:nil PPSBytes:nil];
    
    if (resultCommand.status1Byte == 0x90 && resultCommand.status2Byte == 0x00){
        return ID_OK;
    } else  {
        warn(@"Power On failed - status: %02x%02x", resultCommand.status1Byte, resultCommand.status2Byte);
        return ID_ERROR;
    }
}

- (long)powerOffCard {
    T0Command *resultCommand = (T0Command*)[_device powerOffCard];
    
    logg(@"Power Off Status: %02x%02x", resultCommand.status1Byte, resultCommand.status2Byte);
    
    if (resultCommand.status1Byte == 0x90 && resultCommand.status2Byte == 0x00) {
        logg(@"successful");
        return ID_OK;
    } else  {
        logg(@"failed");
        return ID_ERROR;
    }
}

- (float) CC_NOTIFICATION_TIME_WINDOW   {
    return 2.0f;
}

#pragma mark - Convenience methods

// Converts a string to a ISO7816Command object
+ (ISO7816Command *) ISO7816CommandFromString:(NSString *)str protocol:(protocol) protocol commandId:(CommandId) command_id {
    if(protocol == PCI_PROTOCOL_0)  {
        T0Command *command = [[T0Command alloc] init];
        
        // CLA - 1 byte
        // INS - 1 byte
        // P1 - 1 byte
        // P2 - 1 byte
        // LC - 0, 1 or 3 bytes (NC)
        // Command data - NC bytes
        // LE - 0,1,2 or 3 bytes
        
        int output_length = -1;
        int start_index = 0;
        int end_index = 2;
        NSRange range;
        range.location = start_index;
        range.length = end_index - start_index;
        if(range.location + range.length > [str length])    {
            warn(@"Command is shorter than expected");
            return nil;
        }

        // CLA
        command.classByte = *([CCCommon stringToByteArray:[str substringWithRange:range] outputLength:&output_length]);
        
        // INS
        start_index = end_index;
        end_index += 2;
        range.location = start_index;
        range.length = end_index - start_index;
        if(range.location + range.length > [str length])    {
            warn(@"Command is shorter than expected");
            return nil;
        }        
        command.instructionByte = *([CCCommon stringToByteArray:[str substringWithRange:range] outputLength:&output_length]);
        
        // P1
        start_index = end_index;
        end_index += 2;
        range.location = start_index;
        range.length = end_index - start_index;
        if(range.location + range.length > [str length])    {
            warn(@"Command is shorter than expected");
            return nil;
        }        
        command.parameter1Byte = *([CCCommon stringToByteArray:[str substringWithRange:range] outputLength:&output_length]);
        
        // P2
        start_index = end_index;
        end_index += 2;
        range.location = start_index;
        range.length = end_index - start_index;
        if(range.location + range.length > [str length])    {
            warn(@"Command is shorter than expected");
            return nil;
        }
        
        command.parameter2Byte = *([CCCommon stringToByteArray:[str substringWithRange:range] outputLength:&output_length]);
        
        // LC
        start_index = end_index;
        end_index += 2; // TODO: For now, this is assumed to be 1 byte
        range.location = start_index;
        range.length = end_index - start_index;
        if(range.location + range.length > [str length])    {
            warn(@"Command is shorter than expected");
            return nil;
        }
        
        command.lengthByte = *([CCCommon stringToByteArray:[str substringWithRange:range] outputLength:&output_length]);
        
        // Read next LC bytes
        long lc = strtol([[str substringWithRange:range] UTF8String], NULL, 16);
        start_index = end_index;
        end_index += lc * 2;    // Each byte is two characters
        range.location = start_index;
        range.length = end_index - start_index;
        if(range.location + range.length > [str length])    {
            warn(@"Command is shorter than expected");
            return nil;
        }
        command.dataBytes = [CCCommon stringToData:[str substringWithRange:range]];
        
        command.commandId = command_id;
        
        int length = [str length];
        if(end_index < length)  {
            warn(@"%d character(s) of the command is not used" , length - end_index);
        }
        else if(end_index > length) {
            warn(@"Command is short by %d character(s)", end_index - length);
        }

        // Check that conversion is correct
        NSData *data_input = [CCCommon stringToData:str];
        NSData *data_output = command.bytes;
        if(data_input.length != data_output.length)  {
            err(@"Conversion to T0Command failed, unequal lengths %d != %d", data_input.length , data_output.length);
            return nil;
        }
        if(![data_input isEqualToData:data_output])  {
            err(@"Conversion failed, input and output unequal");
            return nil;
        }

        return command;
    }
    else if(protocol == PCI_PROTOCOL_1) {
        err(@"PCI_PROTOCOL_1 not implemented yet");
    }
    else {
        err(@"Unexpected protocol %d", protocol);
    }
    return nil;
}

#pragma mark - iSmartDelegate methods

// These delegate callbacks are not used
-(void)iSmartDidConnect{
    logg(@"+ iSmartDidConnect");
}

-(void)iSmartDidDisconnect{
    logg(@"+ iSmartDidDisconnect");
}

-(void)cardStatusChanged:(unsigned char)status{
    logg(@"+ iSmart - cardStatusChanged to 0x%02x", status);
}

#pragma mark - SCard methods (pc/sc)

- (LONG) SCardEstablishContextWithDwScope:(DWORD) dwScope pvReserved1:(LPCVOID) pvReserved1 pvReserved2:(LPCVOID) pvReserved2 phContext: (LPSCARDCONTEXT) phContext  {
    warn(@"SCard operations not supported with idtech reader/iSmart SDK");
    return SCARD_E_READER_UNSUPPORTED;
}

- (LONG) SCardIsValidContextWithHContext:(SCARDCONTEXT) hContext    {
    warn(@"SCard operations not supported with idtech reader/iSmart SDK");
    return SCARD_E_READER_UNSUPPORTED;
}

- (LONG) SCardReleaseContextWithHContext:(SCARDCONTEXT) hContext    {
    warn(@"SCard operations not supported with idtech reader/iSmart SDK");
    return SCARD_E_READER_UNSUPPORTED;
}

- (LONG) SCardSetTimeoutWithHContext:(SCARDCONTEXT) hContext dwTimeout:(DWORD) dwTimeout    {
    warn(@"SCard operations not supported with idtech reader/iSmart SDK");
    return SCARD_E_READER_UNSUPPORTED;
}

- (LONG) SCardConnectWithHContext:(SCARDCONTEXT) hContext szReader:(LPCSTR) szReader dwShareMode: (DWORD) dwShareMode dwPreferredProtocols:(DWORD) dwPreferredProtocols phCard:(LPSCARDHANDLE) phCard pdwActiveProtocol:(LPDWORD) pdwActiveProtocol {
    warn(@"SCard operations not supported with idtech reader/iSmart SDK");
    return SCARD_E_READER_UNSUPPORTED;
}

- (LONG) SCardReconnectWithHCard:(SCARDHANDLE) hCard dwShareMode:(DWORD) dwShareMode dwPreferredProtocols:(DWORD) dwPreferredProtocols dwInitialization:(DWORD) dwInitialization pdwActiveProtocol:(LPDWORD) pdwActiveProtocol  {
    warn(@"SCard operations not supported with idtech reader/iSmart SDK");
    return SCARD_E_READER_UNSUPPORTED;
}

- (LONG) SCardDisconnectWithHCard:(SCARDHANDLE) hCard dwDisposition:(DWORD) dwDisposition   {
    warn(@"SCard operations not supported with idtech reader/iSmart SDK");
    return SCARD_E_READER_UNSUPPORTED;
}

- (LONG) SCardBeginTransactionWithHCard:(SCARDHANDLE) hCard {
    warn(@"SCard operations not supported with idtech reader/iSmart SDK");
    return SCARD_E_READER_UNSUPPORTED;
}

- (LONG) SCardEndTransactionWithHCard:(SCARDHANDLE) hCard dwDisposition:(DWORD) dwDisposition   {
    warn(@"SCard operations not supported with idtech reader/iSmart SDK");
    return SCARD_E_READER_UNSUPPORTED;
}

- (LONG) SCardCancelTransactionWithHCard:(SCARDHANDLE) hCard    {
    warn(@"SCard operations not supported with idtech reader/iSmart SDK");
    return SCARD_E_READER_UNSUPPORTED;
}

- (LONG) SCardStatusWithHCard:(SCARDHANDLE) hCard mszReaderNames:(LPSTR) mszReaderNames pcchReaderLen:(LPDWORD) pcchReaderLen pdwState:(LPDWORD) pdwState pdwProtocol:(LPDWORD) pdwProtocol pbAtr:(LPBYTE) pbAtr pcbAtrLen:(LPDWORD) pcbAtrLen  {
    warn(@"SCard operations not supported with idtech reader/iSmart SDK");
    return SCARD_E_READER_UNSUPPORTED;
}

- (LONG) SCardGetStatusChangeWithHContext:(SCARDCONTEXT) hContext dwTimeout:(DWORD) dwTimeout rgReaderStates:(LPSCARD_READERSTATE_A) rgReaderStates cReaders:(DWORD) cReaders   {
    warn(@"SCard operations not supported with idtech reader/iSmart SDK");
    return SCARD_E_READER_UNSUPPORTED;
}

- (LONG) SCardControlWithHCard:(SCARDHANDLE) hCard dwControlCode:(DWORD) dwControlCode pbSendBuffer:(const void *)pbSendBuffer cbSendLength:(DWORD) cbSendLength pbRecvBuffer:(void *)pbRecvBuffer cbRecvLength:(DWORD) cbRecvLength lbBytesReturned:(LPDWORD) lpBytesReturned  {
    warn(@"SCard operations not supported with idtech reader/iSmart SDK");
    return SCARD_E_READER_UNSUPPORTED;
}

- (LONG) SCardTransmitWithHCard:(SCARDHANDLE) hCard pioSendPci:(LPCSCARD_IO_REQUEST) pioSendPci pbSendBuffer:(LPCBYTE) pbSendBuffer cbSendLength:(DWORD) cbSendLength pioRecvPci:(LPSCARD_IO_REQUEST) pioRecvPci pbRecvBuffer:(LPBYTE) pbRecvBuffer pcbRecvLength:(LPDWORD) pcbRecvLength   {
    warn(@"SCard operations not supported with idtech reader/iSmart SDK");
    return SCARD_E_READER_UNSUPPORTED;
}

- (LONG) SCardSecTransmitWithHCard:(SCARDHANDLE) hCard pbSendBuffer:(LPCBYTE) pbSendBuffer cbSendLength:(DWORD) cbSendLength pbRecvBuffer:(LPBYTE) pbRecvBuffer pbRecvLength:(LPDWORD) pcbRecvLength    {
    warn(@"SCard operations not supported with idtech reader/iSmart SDK");
    return SCARD_E_READER_UNSUPPORTED;
}

- (LONG) SCardListReaderGroupsWithHContext:(DWORD) hContext mszGroups:(LPCBYTE *) mszGroups pcchGroups:(LPDWORD *)pcchGroups    {
    warn(@"SCard operations not supported with idtech reader/iSmart SDK");
    return SCARD_E_READER_UNSUPPORTED;
}

- (LONG) SCardListReadersWithHContext:(SCARDCONTEXT) hContext mszGroups:(LPCSTR) mszGroups mszReaders:(LPSTR) mszReaders pcchReaders:(LPDWORD) pcchReaders  {
    warn(@"SCard operations not supported with idtech reader/iSmart SDK");
    return SCARD_E_READER_UNSUPPORTED;
}

- (LONG) SCardCancel:(SCARDCONTEXT) hContext    {
    warn(@"SCard operations not supported with idtech reader/iSmart SDK");
    return SCARD_E_READER_UNSUPPORTED;
}

@end
