//
//  APDUResponse.h
//  checkid
//
//  Created by Ismail Ege AKPINAR on 28/03/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "wintypes.h"

@interface APDUResponse : NSObject

@property (nonatomic, strong) NSData *data; // Data returned from PC/SC call
@property (nonatomic) LONG response_value;  // Response value returned from PC/SC call
@property (nonatomic) uint8_t sw1;
@property (nonatomic) uint8_t sw2;
- (BOOL) isOK;  // Returns YES if response is successful, NO otherwise
- (void) log;   // Convenience method to log contents of the response
@end
