//
//  CardReader_precise.m
//  checkid
//
//  Created by Ismail Ege AKPINAR on 13/03/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import "CardReader_precise.h"

// Import PBAccessory.h to for easy tracking of Tactivo connect / disconnect events.
#import "PBAccessory.h"
// Import the Objective-C smart card functionality header file.
#import "PBSmartcard.h"

#import "Debug.h"
#import "CCCommon.h"

#import "winscard_precise.h"

@interface CardReader_precise()

@property (nonatomic, retain) PBAccessory *accessory;
@property (nonatomic, retain) PBSmartcard *smartcard;

@end

@implementation CardReader_precise  {
    BOOL _is_opened;
}

@synthesize ATR = _ATR;

#pragma mark - CardReader methods

- (NSString *) getATR {
    NSArray *arr = [_smartcard getATR];
    NSMutableData *data = [NSMutableData data];
    for(NSNumber *num in arr)   {

        int num_int = [num intValue];
        [data appendBytes:(__bridge const void *)([NSData dataWithBytes:&num_int length:sizeof(int)]) length:sizeof(int)];
    }
    _ATR = [CCCommon dataToString:data];
    return _ATR;
}

- (protocol) getActiveProtocol    {
    PBSmartcardProtocol protocol = [_smartcard getCurrentProtocol];
    switch (protocol) {
        case PBSmartcardProtocolTx:
        case PBSmartcardProtocolT0:
            return PCI_PROTOCOL_0;
        case PBSmartcardProtocolT1:
            return PCI_PROTOCOL_1;
        default:
            warn(@"Unrecognised protocol");
            return PCI_PROTOCOL_Undefined;
    }
}

- (void) config {
    self.type = precise;
    if(!_smartcard) {
        _smartcard = [[PBSmartcard alloc]init];
    }
    
    if(!_accessory) {
        _accessory = [PBAccessory sharedClass];
    }

    _is_opened = NO;
}

- (void) reset  {
    if(_smartcard)  {
        [_smartcard close];
    }
    _is_opened = NO;
}

- (APDUResponse *) sendCommand:(NSString *)command {
    APDUResponse *response = [[APDUResponse alloc] init];
    
    deb(@"+ sendCommand");
    
//    unsigned char pin_command[] = {0x00, 0x20, 0x00, 0x01, 0x08, 0x24, 0x12, 0x34, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};

    int send_buffer_length;
    unsigned char *send_buffer = [CCCommon stringToByteArray:command outputLength:&send_buffer_length];

    
    unsigned char received_data[255] = {0};
    unsigned short received_data_length;
    
    // on input received_data_length holds the size of the receive buffer.
    received_data_length = sizeof(received_data);
    
    // send the command APDU and get the response from the card.
    PBSmartcardStatus result = [_smartcard transmit:send_buffer
               withCommandLength:send_buffer_length
               andResponseBuffer:received_data
               andResponseLength:&received_data_length];
    
    if (received_data[received_data_length-2] == 0x6C)  {
        // 6Cxx: Bad length value, xx is the correct length
        // Re-send the command with correct Le.
        warn(@"Bad length value, re-sending the command with correct Le");
        
        send_buffer[4] = received_data[received_data_length-1];
        received_data_length = sizeof(received_data);
        
        result = [_smartcard transmit:send_buffer
                   withCommandLength:send_buffer_length
                   andResponseBuffer:received_data
                   andResponseLength:&received_data_length];
        
        logg(@"transmit = %d", result);
    }
    else if (received_data[received_data_length-2] == 0x61) {
        // 61xx : Successful, xx bytes ready to be read
        // Read the remaining data with GET RESPONSE.
        logg(@"Successful, reading remaining data with GET RESPONSE");
        
        unsigned char get_response[] = {0x00, 0xC0, 0x00, 0x00, 0x00};
        get_response[4] = received_data[received_data_length-1];
        received_data_length = sizeof(received_data);
        
        result = [_smartcard transmit:get_response
                   withCommandLength:sizeof(get_response)
                   andResponseBuffer:received_data
                   andResponseLength:&received_data_length];
        
        logg(@"transmit = %d", result);
        
        // check if the command was succefully sent to the card
    }
        
    deb(@"%@",[NSData dataWithBytes:received_data length:received_data_length]);
    
    response.data = [NSData dataWithBytes:received_data length:received_data_length-2];
    response.sw1 = received_data[received_data_length - 2];
    response.sw2 = received_data[received_data_length - 1];
    
    switch(result)  {
        case PBSmartcardStatusSuccess:
            deb(@"Send command - successful");
            break;
        case PBSmartcardStatusInternalSessionLost:
        case PBSmartcardStatusInvalidParameter:
        case PBSmartcardStatusInvalidValue:
        case PBSmartcardStatusNoSmartcard:
        case PBSmartcardStatusNotConnected:
        case PBSmartcardStatusNotReady:
        case PBSmartcardStatusNotSupported:
        case PBSmartcardStatusProtocolMismatch:
        case PBSmartcardStatusProtocolNotIncluded:
        case PBSmartcardStatusReaderUnavailable:
        case PBSmartcardStatusRemovedCard:
        case PBSmartcardStatusResetCard:
        case PBSmartcardStatusSharingViolation:
        case PBSmartcardStatusUnexpected:
        case PBSmartcardStatusUnpoweredCard:
        case PBSmartcardStatusUnresponsiveCard:
        case PBSmartcardStatusUnsupportedCard:
            logg(@"Known error code - %d", result);
            break;
        default:
            warn(@"Unrecognised error code - %d",result);
    }
    return response;
}

- (long) connectCard {
    
    PBSmartcardStatus result;
    
    // open the reader/card object to enable card notifications
    if(!_is_opened)  {
        _is_opened = YES;
        result = [_smartcard open];
        
        if (result != PBSmartcardStatusSuccess) {
            return ID_READER_CONTEXT_ERROR;
        }
        
        if([_smartcard getSlotStatus] == PBSmartcardSlotStatusEmpty)    {
            return ID_CARD_ABSENT;
        }        
    }
    

    // While loop placeholder
    
    // connect to the card using the first protocol offered by the card
    result = [_smartcard connect:PBSmartcardProtocolTx];
    
    if (result == PBSmartcardStatusNoSmartcard) {
        return  ID_CARD_ABSENT;
    }
    else if (result != PBSmartcardStatusSuccess)    {
        return ID_ERROR;
    }
    else    {
        // Unpower the card to to reduce battery drain.
        result = [_smartcard disconnect:PBSmartcardDispositionUnpowerCard];
        
        return ID_CARD_ACCESS_SUCCESSFUL;
    }
}

- (long) powerOnCard {
    deb(@"+ powerOnCard");
    PBSmartcardStatus result = [_smartcard connect:PBSmartcardProtocolTx];
    
    if (result == PBSmartcardStatusNoSmartcard) {
        err(@"Power ON failed - card not found");
        return  ID_CARD_ABSENT;
    }
    else if (result != PBSmartcardStatusSuccess)    {
        err(@"Power ON failed");
        return ID_ERROR;
    }
    else    {
        logg(@"Power ON successful");
        return ID_OK;
    }    
}

- (long) powerOffCard    {
    PBSmartcardStatus result = [_smartcard disconnect:PBSmartcardDispositionUnpowerCard];
    if(result == PBSmartcardStatusSuccess)  {
        deb(@"Power OFF successful");
        return ID_OK;
    }
    else {
        warn(@"Power OFF failed");
        return ID_ERROR;
    }
}

#pragma mark - SCard methods (pc/sc)

- (LONG) SCardEstablishContextWithDwScope:(DWORD) dwScope pvReserved1:(LPCVOID) pvReserved1 pvReserved2:(LPCVOID) pvReserved2 phContext: (LPSCARDCONTEXT) phContext  {
    return SCa03EstablishContext(dwScope, pvReserved1, pvReserved2, phContext);
}

- (LONG) SCardIsValidContextWithHContext:(SCARDCONTEXT) hContext    {
    return SCa03IsValidContext(hContext);
}

- (LONG) SCardReleaseContextWithHContext:(SCARDCONTEXT) hContext    {
    return SCa03ReleaseContext(hContext);
}

- (LONG) SCardSetTimeoutWithHContext:(SCARDCONTEXT) hContext dwTimeout:(DWORD) dwTimeout    {
    warn(@"Unimplemented");
    return SCARD_E_READER_UNSUPPORTED;
}

- (LONG) SCardConnectWithHContext:(SCARDCONTEXT) hContext szReader:(LPCSTR) szReader dwShareMode: (DWORD) dwShareMode dwPreferredProtocols:(DWORD) dwPreferredProtocols phCard:(LPSCARDHANDLE) phCard pdwActiveProtocol:(LPDWORD) pdwActiveProtocol {
    return SCa03Connect(hContext, szReader, dwShareMode, dwPreferredProtocols, phCard, pdwActiveProtocol);
}

- (LONG) SCardReconnectWithHCard:(SCARDHANDLE) hCard dwShareMode:(DWORD) dwShareMode dwPreferredProtocols:(DWORD) dwPreferredProtocols dwInitialization:(DWORD) dwInitialization pdwActiveProtocol:(LPDWORD) pdwActiveProtocol  {
    return SCa03Reconnect(hCard, dwShareMode, dwPreferredProtocols, dwInitialization, pdwActiveProtocol);
}

- (LONG) SCardDisconnectWithHCard:(SCARDHANDLE) hCard dwDisposition:(DWORD) dwDisposition   {
    return SCa03Disconnect(hCard, dwDisposition);
}

- (LONG) SCardBeginTransactionWithHCard:(SCARDHANDLE) hCard {
    return SCa03BeginTransaction(hCard);
}

- (LONG) SCardEndTransactionWithHCard:(SCARDHANDLE) hCard dwDisposition:(DWORD) dwDisposition   {
    return SCa03EndTransaction(hCard, dwDisposition);
}

- (LONG) SCardCancelTransactionWithHCard:(SCARDHANDLE) hCard    {
    warn(@"Unimplemented");
    return SCARD_E_READER_UNSUPPORTED;
}

- (LONG) SCardStatusWithHCard:(SCARDHANDLE) hCard mszReaderNames:(LPSTR) mszReaderNames pcchReaderLen:(LPDWORD) pcchReaderLen pdwState:(LPDWORD) pdwState pdwProtocol:(LPDWORD) pdwProtocol pbAtr:(LPBYTE) pbAtr pcbAtrLen:(LPDWORD) pcbAtrLen  {
    return SCa03Status(hCard, mszReaderNames, pcchReaderLen, pdwState, pdwProtocol, pbAtr, pcbAtrLen);
}

- (LONG) SCardGetStatusChangeWithHContext:(SCARDCONTEXT) hContext dwTimeout:(DWORD) dwTimeout rgReaderStates:(LPSCARD_READERSTATE_A) rgReaderStates cReaders:(DWORD) cReaders   {
    return SCa03GetStatusChange(hContext, dwTimeout, rgReaderStates, cReaders);
}

- (LONG) SCardControlWithHCard:(SCARDHANDLE) hCard dwControlCode:(DWORD) dwControlCode pbSendBuffer:(const void *)pbSendBuffer cbSendLength:(DWORD) cbSendLength pbRecvBuffer:(void *)pbRecvBuffer cbRecvLength:(DWORD) cbRecvLength lbBytesReturned:(LPDWORD) lpBytesReturned  {
    return SCa03Control(hCard, dwControlCode, pbSendBuffer, cbSendLength, pbRecvBuffer, cbRecvLength, lpBytesReturned);
}

- (LONG) SCardTransmitWithHCard:(SCARDHANDLE) hCard pioSendPci:(LPCSCARD_IO_REQUEST) pioSendPci pbSendBuffer:(LPCBYTE) pbSendBuffer cbSendLength:(DWORD) cbSendLength pioRecvPci:(LPSCARD_IO_REQUEST) pioRecvPci pbRecvBuffer:(LPBYTE) pbRecvBuffer pcbRecvLength:(LPDWORD) pcbRecvLength   {
    return SCa03Transmit(hCard, pioSendPci, pbSendBuffer, cbSendLength, pioRecvPci, pbRecvBuffer, pcbRecvLength);
}

- (LONG) SCardSecTransmitWithHCard:(SCARDHANDLE) hCard pbSendBuffer:(LPCBYTE) pbSendBuffer cbSendLength:(DWORD) cbSendLength pbRecvBuffer:(LPBYTE) pbRecvBuffer pbRecvLength:(LPDWORD) pcbRecvLength    {
    warn(@"Unimplemented");
    return SCARD_E_READER_UNSUPPORTED;
}

- (LONG) SCardListReaderGroupsWithHContext:(DWORD) hContext mszGroups:(LPCBYTE *) mszGroups pcchGroups:(LPDWORD *)pcchGroups    {
    warn(@"Unimplemented");
    return SCARD_E_READER_UNSUPPORTED;
}

- (LONG) SCardListReadersWithHContext:(SCARDCONTEXT) hContext mszGroups:(LPCSTR) mszGroups mszReaders:(LPSTR) mszReaders pcchReaders:(LPDWORD) pcchReaders  {
    return SCa03ListReaders(hContext, mszGroups, mszReaders, pcchReaders);
}

- (LONG) SCardCancel:(SCARDCONTEXT) hContext    {
    return SCa03Cancel(hContext);
}

@end
