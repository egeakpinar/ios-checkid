//
//  CardReader.h
//  checkid
//
//  Created by Ismail Ege AKPINAR on 13/03/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "APDUResponse.h"
#import "../../lib/winscard.h"
#import "../../lib/wintypes.h"
#import "IDCodes.h"

@class Card;

typedef enum {
    identity,
    idtech,
    precise,
    feitian,
    bai
} reader_type;

typedef enum {
    PCI_PROTOCOL_0,
    PCI_PROTOCOL_1,
    PCI_PROTOCOL_Raw,
    PCI_PROTOCOL_Undefined
} protocol;

@interface CardReader : NSObject

#pragma mark - Public properties
@property(nonatomic, assign) reader_type type;
@property (nonatomic, retain, getter = getCard) Card *card;
@property(nonatomic, assign, getter = getActiveProtocol) protocol active_protocol;  // Card must be powered ON before protocol is determined
@property(nonatomic, copy , getter = getATR , readonly) NSString *ATR;

#pragma mark - Public methods
- (long) connectCard;    // Used to get status of connection (e.g. reader connected, card not found)
- (long) powerOnCard;
- (long) powerOffCard;
- (APDUResponse *) sendCommand:(NSString *) command;

// Applicable for BAI only
- (void) showPairingGUI:(UINavigationController *) nav_controller callback:(void (BOOL success))callback;

- (void) config;    // Called to initialise configuration
- (void) reset; // Called when reader is disconnected to reset state


#pragma mark - SCard methods (pc/sc)

- (LONG) SCardEstablishContextWithDwScope:(DWORD) dwScope pvReserved1:(LPCVOID) pvReserved1 pvReserved2:(LPCVOID) pvReserved2 phContext: (LPSCARDCONTEXT) phContext;

- (LONG) SCardIsValidContextWithHContext:(SCARDCONTEXT) hContext;

- (LONG) SCardReleaseContextWithHContext:(SCARDCONTEXT) hContext;


- (LONG) SCardSetTimeoutWithHContext:(SCARDCONTEXT) hContext dwTimeout:(DWORD) dwTimeout;

- (LONG) SCardConnectWithHContext:(SCARDCONTEXT) hContext szReader:(LPCSTR) szReader dwShareMode: (DWORD) dwShareMode dwPreferredProtocols:(DWORD) dwPreferredProtocols phCard:(LPSCARDHANDLE) phCard pdwActiveProtocol:(LPDWORD) pdwActiveProtocol;

- (LONG) SCardReconnectWithHCard:(SCARDHANDLE) hCard dwShareMode:(DWORD) dwShareMode dwPreferredProtocols:(DWORD) dwPreferredProtocols dwInitialization:(DWORD) dwInitialization pdwActiveProtocol:(LPDWORD) pdwActiveProtocol;

- (LONG) SCardDisconnectWithHCard:(SCARDHANDLE) hCard dwDisposition:(DWORD) dwDisposition;

- (LONG) SCardBeginTransactionWithHCard:(SCARDHANDLE) hCard;

- (LONG) SCardEndTransactionWithHCard:(SCARDHANDLE) hCard dwDisposition:(DWORD) dwDisposition;

- (LONG) SCardCancelTransactionWithHCard:(SCARDHANDLE) hCard;

- (LONG) SCardStatusWithHCard:(SCARDHANDLE) hCard mszReaderNames:(LPSTR) mszReaderNames pcchReaderLen:(LPDWORD) pcchReaderLen pdwState:(LPDWORD) pdwState pdwProtocol:(LPDWORD) pdwProtocol pbAtr:(LPBYTE) pbAtr pcbAtrLen:(LPDWORD) pcbAtrLen;

- (LONG) SCardGetStatusChangeWithHContext:(SCARDCONTEXT) hContext dwTimeout:(DWORD) dwTimeout rgReaderStates:(LPSCARD_READERSTATE_A) rgReaderStates cReaders:(DWORD) cReaders;

- (LONG) SCardControlWithHCard:(SCARDHANDLE) hCard dwControlCode:(DWORD) dwControlCode pbSendBuffer:(const void *)pbSendBuffer cbSendLength:(DWORD) cbSendLength pbRecvBuffer:(void *)pbRecvBuffer cbRecvLength:(DWORD) cbRecvLength lbBytesReturned:(LPDWORD) lpBytesReturned;

- (LONG) SCardTransmitWithHCard:(SCARDHANDLE) hCard pioSendPci:(LPCSCARD_IO_REQUEST) pioSendPci pbSendBuffer:(LPCBYTE) pbSendBuffer cbSendLength:(DWORD) cbSendLength pioRecvPci:(LPSCARD_IO_REQUEST) pioRecvPci pbRecvBuffer:(LPBYTE) pbRecvBuffer pcbRecvLength:(LPDWORD) pcbRecvLength;

- (LONG) SCardSecTransmitWithHCard:(SCARDHANDLE) hCard pbSendBuffer:(LPCBYTE) pbSendBuffer cbSendLength:(DWORD) cbSendLength pbRecvBuffer:(LPBYTE) pbRecvBuffer pbRecvLength:(LPDWORD) pcbRecvLength;

- (LONG) SCardListReaderGroupsWithHContext:(DWORD) hContext mszGroups:(LPCBYTE *) mszGroups pcchGroups:(LPDWORD *)pcchGroups;

- (LONG) SCardListReadersWithHContext:(SCARDCONTEXT) hContext mszGroups:(LPCSTR) mszGroups mszReaders:(LPSTR) mszReaders pcchReaders:(LPDWORD) pcchReaders;

- (LONG) SCardCancel:(SCARDCONTEXT) hContext;

- (float) CC_NOTIFICATION_TIME_WINDOW;   // Allow x s before a notification is sent (used to pool notifications in case there are many and only sends the most recent one)

@end
