//
//  CardReader_bai.m
//  checkid
//
//  Created by Ismail Ege AKPINAR on 19/03/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import "CardReader_bai.h"
#import "Debug.h"
#import "CCCommon.h"

// BAI SDK Modules
#import "BAIModuleConnect.h"
#import "BAIModulePairing.h"

// Middleware Module
#import "pkcs11.h"
#import "balPkcs11.h" // Middleware provides a subset of the pkcs#11 interface. See this header for details
//#import "mapPkcs11.h" // Maps normal pkcs#11 names to BAL specific implementation
//#import "pcsclite.h"
#import "winscard_bai.h"
#import "wintypes.h"

#import "Card.h"

@implementation CardReader_bai
@synthesize ATR = _ATR;

#pragma mark - CardReader properties

- (NSData *) getATR {
    // Note: bai doesn't support SCardStatus (as stated on baiwiki.com) so this call might be unreliable
    if(!_ATR)   {
        DWORD dwReaderLen = MAX_READERNAME;
        char* pcReaders = (char *) malloc(sizeof(char) * MAX_READERNAME);
        DWORD dwAtrLen = MAX_ATR_SIZE;
        DWORD dwState,dwProt;
        unsigned char pbAtr[MAX_ATR_SIZE];
        
        LONG rv = SCa01Status(_handle_scard, pcReaders, &dwReaderLen, &dwState, &dwProt,
                         pbAtr, &dwAtrLen);
        NSData *atr_data = [NSData dataWithBytes:pbAtr length:dwAtrLen];
        _ATR = [CCCommon dataToString:atr_data];
    }
    return _ATR;
}

// NOTE: bai doesn't support destroying context. For this reason, bai uses a single static context for all connections

static hBAIContext *_context_bai;
static unsigned long _hSession;
static SCARDCONTEXT _context_scard; // TODO: Check if they have to be static as well
static SCARDHANDLE _handle_scard;

- (long) verifyPIN:(NSString *)PIN   {
    BOOL is_successful = NO;
    unsigned long hSession;
    if (BAL_C_Initialize(NULL)) {
        if ([self p11OpenSession:&hSession] == ID_OK)  {
            // Obtain the PIN, provide it for login
            
            const unsigned char *bValue = (const unsigned char *) [PIN cStringUsingEncoding:NSASCIIStringEncoding];
            
            unsigned int providedPINLength = [PIN length];
            unsigned char* providedPIN = malloc(providedPINLength);
            memcpy(providedPIN, bValue, providedPINLength);
            
            CK_RV rv = BAL_C_Login(hSession,CKU_USER,providedPIN,providedPINLength);
            if(rv == CKR_PIN_LOCKED)    {
                err(@"PIN locked");
            }
            else if(rv == CKR_PIN_INCORRECT)    {
                warn(@"PIN incorrect");
            }
            else if(rv == CKR_PIN_INVALID)  {
                warn(@"PIN invalid");
            }
            else if(rv == CKR_PIN_EXPIRED)  {
                warn(@"PIN expired");
            }
            else if(rv == CKR_OK)   {
                logg(@"PIN verification successful");
                is_successful = YES;
            }
            else    {
                warn(@"Unexpected PIN response");
            }
            
            BAL_C_CloseSession(hSession);
        }
        BAL_C_Finalize(NULL);
    }
    if(is_successful)   {
        return ID_OK;
    }
    return ID_ERROR;
}

- (long)connectCard  {
    int _readerCount = 0;
    SCARD_READERSTATE _rgReaderStates[1];
    
    LONG rv = SCa01GetStatusChange(_context_scard, 0, _rgReaderStates, _readerCount);
	if(rv!=SCARD_S_SUCCESS) {
        return ID_ERROR;
    } else {
        // Try connect to card here
        if(_rgReaderStates[0].dwEventState & SCARD_STATE_PRESENT)    {
            return ID_CARD_ACCESS_SUCCESSFUL;
        } else {
            logg(@"no card");
            
            // Try connect
            bool b = 0;
            // Note: This method takes VERY long (5-8 seconds) when there is no card in the reader
            BAIRESPONSE rval = bai_interface_connect(_context_bai, 0, NULL, NULL, &b);
            if(rval == 0)   {
                logg(@"connect successful");
                return ID_CARD_ACCESS_SUCCESSFUL;
            }
            return ID_CARD_ABSENT;
        }
    }
}


- (long) powerOnCard {
    
	LONG rv = 0;
    DWORD dwActiveProtocol = -1;
    char mszReaders[128] = "";
    DWORD dwReaders = -1;
    
    rv = SCa01ListReaders(_context_scard, NULL, mszReaders, &dwReaders);
    if(rv != SCARD_S_SUCCESS) {
        err(@"SCardListReaders error %08x",rv);
        return ID_ERROR;
    }
    
    logg(@"mszReaders %s", mszReaders);
    
    rv = SCa01Connect(_context_scard, mszReaders,
					  SCARD_SHARE_SHARED, SCARD_PROTOCOL_T0,
					  &_handle_scard, &dwActiveProtocol);
    
    switch (dwActiveProtocol) {
        case SCARD_PROTOCOL_ANY:
        case SCARD_PROTOCOL_T0:
            self.active_protocol = PCI_PROTOCOL_0;
            break;
        case SCARD_PROTOCOL_T1:
            self.active_protocol = PCI_PROTOCOL_1;
            break;
        case SCARD_PROTOCOL_RAW:
            self.active_protocol = PCI_PROTOCOL_Raw;
            break;
        default:
            warn(@"Unrecognised protocol");
            self.active_protocol = PCI_PROTOCOL_Undefined;
    }
    
	if(rv!=SCARD_S_SUCCESS) {
        // NOTE: For card status, we are always relying on connectCard response for sake of simplicity (separation of concerns)
        err(@"SCardConnect error %x", rv);
        return ID_ERROR;
	}
	else if(!_handle_scard)   {
        return ID_CARD_CONNECT_ERROR;
    }
    
    // Force open bluetooth connection (it's already opened actually)
    bool b = 0;
    // Note: This method takes VERY long (5-8 seconds) when there is no card in the reader
    BAIRESPONSE rval = bai_interface_connect(_context_bai, 0, NULL, NULL, &b);
    if(rval != 0)   {
        logg(@"opening bluetooth connection failed %x",rv);
        return ID_ERROR;
    }
    
    [self.card verifyPIN:@"1234"];
    [self powerOffCard];
    
    return ID_OK;
    /* bai doesn't support SCardStatus (although correct ATR is fetched)
    DWORD dwReaderLen = MAX_READERNAME;
    char* pcReaders = (char *) malloc(sizeof(char) * MAX_READERNAME);
    DWORD dwAtrLen = MAX_ATR_SIZE;
    DWORD dwState,dwProt;
    unsigned char pbAtr[MAX_ATR_SIZE];
    
    rv = SCa01Status(_handle_scard, pcReaders, &dwReaderLen, &dwState, &dwProt,
                     pbAtr, &dwAtrLen);
    logg(@"ATR %@", [NSData dataWithBytes:pbAtr length:dwAtrLen]);
    logg(@"Current reader protocol %d", dwProt - 1);
    
    if(rv != SCARD_S_SUCCESS) {
        warn(@"SCardStatus failed %x", rv);
        return cc_response_Error;
    }
    return cc_response_OK;*/
}


- (long) powerOffCard    {
    LONG rv;
    if(_handle_scard)  {
        rv = SCa01Disconnect(_handle_scard, SCARD_RESET_CARD);
        if(rv != CKR_OK)    {
            warn(@"SCardDisconnect failed %x", rv);
        }
        _handle_scard = 0;
    }
    // rv is ignored, assuming success
    return ID_OK;
}

- (APDUResponse *) sendCommand:(NSString *)command {
    APDUResponse *response = [[APDUResponse alloc] init];
	uint8_t pbRecvBuffer[512];
	DWORD cbRecvLength=512;
	LONG rv;
	SCARD_IO_REQUEST pioRecvPci;
 	SCARD_IO_REQUEST pioSendPci;

    switch (self.active_protocol) {
        case PCI_PROTOCOL_0:
            pioSendPci = *SCARD_PCI_T0;
            break;
        case PCI_PROTOCOL_1:
            pioSendPci = *SCARD_PCI_T1;
            break;
        case PCI_PROTOCOL_Raw:
            pioSendPci = *SCARD_PCI_RAW;
            break;
        case PCI_PROTOCOL_Undefined:
        default:
            warn(@"Active protocol not set");
            break;
    }
    
    int pbSendLength = -1;
    uint8_t *pbSendBuffer = [CCCommon stringToByteArray:command outputLength:&pbSendLength];
    if(pbSendBuffer == NULL)    {
        err(@"Could not create command bytes");
        return nil;
	}
    
	rv=SCa01Transmit(_handle_scard,&pioSendPci,
					 pbSendBuffer, pbSendLength,
					 (const unsigned char*)&pioRecvPci,
                     pbRecvBuffer,
					 &cbRecvLength);
    response.response_value = rv;
    
	if(rv==SCARD_S_SUCCESS) {
        logg(@"SCardTransmit successful");
        logg(@"%@",[NSData dataWithBytes:pbRecvBuffer length:cbRecvLength]);
        response.data = [NSData dataWithBytes:pbRecvBuffer length:cbRecvLength-2];
        response.sw1 = pbRecvBuffer[cbRecvLength - 2];
        response.sw2 = pbRecvBuffer[cbRecvLength - 1];
	} else {
        err(@"SCardTransmit Error:%x", rv);
	}
    
    return response;
}

- (void) config {
    super.type = bai;
    if(!_context_bai)   {
        bai_interface_create_context(&_context_bai, NULL);
    }
    if(!_context_scard) {
        LONG rv = SCa01EstablishContext(SCARD_SCOPE_SYSTEM, NULL, NULL, &_context_scard);
        if(rv)  {
            logg(@"SCardEstablishContext Error:0x%x",rv);
        }
        else    {
            logg(@"SCardEstablishContext Successful");
        }
        
    }
    // The below calls are for the pairing GUI module
    // TODO: Re-think whether you need them    
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(pairingFinishedNotificationObserved:)
												 name:BAIPairingDidFinishNotification
											   object:nil];
}

- (void) reset  {
    // NOTE: bai doesn't support destroying bai context
	if(_context_scard)    {
		SCa01ReleaseContext(_context_scard);
        _context_scard = 0;
        _handle_scard = 0;
	}
}

/*
- (cc_response) powerOnCard {
    warn(@"Power ON not implemented for bai, yet");
    return cc_response_Error;    
}

- (cc_response) powerOffCard    {
    warn(@"Power OFF not implemented for bai, yet");
    return cc_response_Error;
}*/

#pragma mark - SDK notification handlers

- (void) navigationControllerRequested:(NSNotification*)note {
    warn(@"nav controller requested");
    // TODO: Current architecture doesn't give you access to the navigation controller. Maybe you should
    // pass in a reference to the nav controller for bai SDK
//	bai_interface_set_navigation_controller(self.na, [note object]);
}

- (void) pairingFinishedNotificationObserved:(NSNotification*)note {
    warn(@"pairingFinishedNotificationObserved");
	BOOL success = [[note object] boolValue];
}


#pragma mark - p11 methods

- (long) p11OpenSession:(unsigned long *) pSession {
    // ** p11OpenSession
    
    CK_SLOT_ID* pSlotList = 0;
    unsigned long ulSlotCount = 0;
    
    // Get slot list for memory allocation
    CK_RV rv = BAL_C_GetSlotList(false, 0, &ulSlotCount);
    
    if ((rv == CKR_OK) && (ulSlotCount > 0))    {
        logg(@"  %d slots found",ulSlotCount);
        pSlotList = (CK_SLOT_ID*)malloc(ulSlotCount * sizeof (CK_SLOT_ID));
        
        if (pSlotList == NULL)  {
            logg(@"System error: unable to allocate memory");
            return ID_READER_CONTEXT_ERROR;
        }
        
        // Get the slot list for processing
        rv = BAL_C_GetSlotList(false, pSlotList, &ulSlotCount);
        if (rv != CKR_OK)
        {
            logg(@"GetSlotList failed: unable to get slot list. Error code 0x%X",rv);
            free(pSlotList);
            return ID_READER_CONTEXT_ERROR;
        }
    }
    else    {
        logg(@"GetSlotList failed: unable to get slot count. Error code 0x%X",rv);
        return ID_READER_CONTEXT_ERROR;
    }
    
    // TODO - use C_GetTokenInfo and check against the flags for the operation we wish to support
    // For now, use the first available slot
    CK_SLOT_ID SlotID;
    if (pSlotList[0] != 0)  {
        SlotID = pSlotList[0];
    }
    else    {
        logg(@"Unable to obtain a valid slot ID");
        return ID_READER_CONTEXT_ERROR;
    }
    
    // Establish a bluetooth connection
    bool b = 0;
    BAIRESPONSE rval = bai_interface_connect(_context_bai, 0, NULL, NULL, &b);
    if (rval != 0)  {
        logg(@"bai_interface_connect failed 0x%X", rval);
        return ID_READER_PROBLEM;
    }
    
    rv = BAL_C_OpenSession(SlotID,0,NULL,NULL,pSession);
    
    // Free allocated memory
    if (pSlotList)  {
        free(pSlotList);
    }
    
    if(rv == CKR_OK)  {
         return ID_OK;
    }
    return ID_ERROR;
}

- (void) showPairingGUI:(UINavigationController *)nav_controller callback:(void (BOOL))callback {
	bai_interface_show_pairing_view(_context_bai, nav_controller, callback);
}

#pragma mark - SCard methods (pc/sc)

- (LONG) SCardEstablishContextWithDwScope:(DWORD) dwScope pvReserved1:(LPCVOID) pvReserved1 pvReserved2:(LPCVOID) pvReserved2 phContext: (LPSCARDCONTEXT) phContext  {
    return SCa01EstablishContext(dwScope, pvReserved1, pvReserved2, phContext);
}

- (LONG) SCardIsValidContextWithHContext:(SCARDCONTEXT) hContext    {
    return SCa01IsValidContext(hContext);
}

- (LONG) SCardReleaseContextWithHContext:(SCARDCONTEXT) hContext    {
    return SCa01ReleaseContext(hContext);
}

- (LONG) SCardSetTimeoutWithHContext:(SCARDCONTEXT) hContext dwTimeout:(DWORD) dwTimeout    {
    warn(@"Unimplemented");
    return SCARD_E_READER_UNSUPPORTED;
}

// TODO: Reuse existing context here
- (LONG) SCardConnectWithHContext:(SCARDCONTEXT) hContext szReader:(LPCSTR) szReader dwShareMode: (DWORD) dwShareMode dwPreferredProtocols:(DWORD) dwPreferredProtocols phCard:(LPSCARDHANDLE) phCard pdwActiveProtocol:(LPDWORD) pdwActiveProtocol {
    return SCa01Connect(hContext, szReader, dwShareMode, dwPreferredProtocols, phCard, pdwActiveProtocol);
}

- (LONG) SCardReconnectWithHCard:(SCARDHANDLE) hCard dwShareMode:(DWORD) dwShareMode dwPreferredProtocols:(DWORD) dwPreferredProtocols dwInitialization:(DWORD) dwInitialization pdwActiveProtocol:(LPDWORD) pdwActiveProtocol  {
    return SCa01Reconnect(hCard, dwShareMode, dwPreferredProtocols, dwInitialization, pdwActiveProtocol);
}

- (LONG) SCardDisconnectWithHCard:(SCARDHANDLE) hCard dwDisposition:(DWORD) dwDisposition   {
    return SCa01Disconnect(hCard, dwDisposition);
}

- (LONG) SCardBeginTransactionWithHCard:(SCARDHANDLE) hCard {
    return SCa01BeginTransaction(hCard);
}

- (LONG) SCardEndTransactionWithHCard:(SCARDHANDLE) hCard dwDisposition:(DWORD) dwDisposition   {
    return SCa01EndTransaction(hCard, dwDisposition);
}

- (LONG) SCardCancelTransactionWithHCard:(SCARDHANDLE) hCard    {
    warn(@"Unimplemented");
    return SCARD_E_READER_UNSUPPORTED;
}

- (LONG) SCardStatusWithHCard:(SCARDHANDLE) hCard mszReaderNames:(LPSTR) mszReaderNames pcchReaderLen:(LPDWORD) pcchReaderLen pdwState:(LPDWORD) pdwState pdwProtocol:(LPDWORD) pdwProtocol pbAtr:(LPBYTE) pbAtr pcbAtrLen:(LPDWORD) pcbAtrLen  {
    return SCa01Status(hCard, mszReaderNames, pcchReaderLen, pdwState, pdwProtocol, pbAtr, pcbAtrLen);
}

- (LONG) SCardGetStatusChangeWithHContext:(SCARDCONTEXT) hContext dwTimeout:(DWORD) dwTimeout rgReaderStates:(LPSCARD_READERSTATE_A) rgReaderStates cReaders:(DWORD) cReaders   {
    return SCa01GetStatusChange(hContext, dwTimeout, rgReaderStates, cReaders);
}

- (LONG) SCardControlWithHCard:(SCARDHANDLE) hCard dwControlCode:(DWORD) dwControlCode pbSendBuffer:(const void *)pbSendBuffer cbSendLength:(DWORD) cbSendLength pbRecvBuffer:(void *)pbRecvBuffer cbRecvLength:(DWORD) cbRecvLength lbBytesReturned:(LPDWORD) lpBytesReturned  {
    return SCa01Control(hCard, dwControlCode, pbSendBuffer, cbSendLength, pbRecvBuffer, cbRecvLength, lpBytesReturned);
}

- (LONG) SCardTransmitWithHCard:(SCARDHANDLE) hCard pioSendPci:(LPCSCARD_IO_REQUEST) pioSendPci pbSendBuffer:(LPCBYTE) pbSendBuffer cbSendLength:(DWORD) cbSendLength pioRecvPci:(LPSCARD_IO_REQUEST) pioRecvPci pbRecvBuffer:(LPBYTE) pbRecvBuffer pcbRecvLength:(LPDWORD) pcbRecvLength   {
    return SCa01Transmit(hCard, pioSendPci, pbSendBuffer, cbSendLength, pioRecvPci, pbRecvBuffer, pcbRecvLength);
}

- (LONG) SCardSecTransmitWithHCard:(SCARDHANDLE) hCard pbSendBuffer:(LPCBYTE) pbSendBuffer cbSendLength:(DWORD) cbSendLength pbRecvBuffer:(LPBYTE) pbRecvBuffer pbRecvLength:(LPDWORD) pcbRecvLength    {
    warn(@"Unimplemented");
    return SCARD_E_READER_UNSUPPORTED;
}

- (LONG) SCardListReaderGroupsWithHContext:(DWORD) hContext mszGroups:(LPCBYTE *) mszGroups pcchGroups:(LPDWORD *)pcchGroups    {
    warn(@"Unimplemented");
    return SCARD_E_READER_UNSUPPORTED;

}

- (LONG) SCardListReadersWithHContext:(SCARDCONTEXT) hContext mszGroups:(LPCSTR) mszGroups mszReaders:(LPSTR) mszReaders pcchReaders:(LPDWORD) pcchReaders  {
    return SCa01ListReaders(hContext, mszGroups, mszReaders, pcchReaders);
}

- (LONG) SCardCancel:(SCARDCONTEXT) hContext    {
    return SCa01Cancel(hContext);
}

@end
