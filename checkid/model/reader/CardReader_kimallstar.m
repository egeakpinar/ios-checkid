//
//  CardReader_kimallstar.m
//  checkid
//
//  Created by Ismail Ege AKPINAR on 13/03/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import "CardReader_kimallstar.h"
#import "Debug.h"
#import "Common.h"

#import "winscard_kimallstar.h"
#import "wintypes.h"

#import "Card_eid.h"

@implementation CardReader_kimallstar {
    SCARDHANDLE _hCard;
	SCARDCONTEXT _hContext;
	DWORD _dwPref;
    
    int _currentReaderStatus;
    int _readerCount;
    SCARD_READERSTATE _rgReaderStates[1];
    
    BOOL _is_first_time;
}

@synthesize ATR = _ATR;

#pragma mark - CardReader properties

#pragma mark - CardReader methods
- (void) config {
    logg(@"+ config");
    self.type = identity;
    _is_first_time = YES;
    if(_hContext==0) {
        LONG rv = SCa02EstablishContext(SCARD_SCOPE_SYSTEM, NULL, NULL, &_hContext);
        if(rv)  {
            logg(@"SCardEstablishContext Error:0x%x",rv);
        }
        else    {
            logg(@"SCardEstablishContext Successful");
        }
    }
    else    {
        logg(@"SCardEstablishContext already initialized ?");
    }    
}

- (void) reset  {
	if(_hContext)    {
		SCa02ReleaseContext(_hContext);
		_hContext=0;
		_hCard=0;
	}
    _is_first_time = YES;
}

- (cc_response)connectCard {
    LONG rv;
    
    if(_is_first_time)   {
        _readerCount = 0;
        if(!_hContext)   {
            rv = SCa02EstablishContext(SCARD_SCOPE_SYSTEM, NULL, NULL, &_hContext);
            if(rv)
            {
                return cc_response_ContextError;
            }
            else    {
                logg(@"SCardEstablishContext successful");
            }
        }
        
        // Get reader name
        char mszReaders[128] = "";
        DWORD dwReaders = -1;
                
        rv = SCa02ListReaders(_hContext, NULL, mszReaders, &dwReaders);
        if(rv != SCARD_S_SUCCESS) {
            err(@"SCardListReaders error %08x",rv);
        }
        
        logg(@"Reader name %s", mszReaders);

        rv=SCa02GetStatusChange(_hContext,INFINITE, 0, _readerCount);
     
        if(rv!=SCARD_S_SUCCESS) {
            return cc_response_CardError;
        } else {
            // No card, continue below
        }
        
        _readerCount=1;
        _rgReaderStates[0].szReader = mszReaders;
        _rgReaderStates[0].dwCurrentState = SCARD_STATE_EMPTY;
        
        _is_first_time = NO;
    }

    rv = SCa02GetStatusChange(_hContext, INFINITE, _rgReaderStates, _readerCount);
	if(rv!=SCARD_S_SUCCESS) {
        _readerCount=0;
        _rgReaderStates[0].dwCurrentState=SCARD_STATE_EMPTY;
        return cc_response_ReaderNotFound;
    } else {
        _readerCount=1;        
        if(_rgReaderStates[0].dwEventState & SCARD_STATE_PRESENT)    {
            _rgReaderStates[0].dwCurrentState=SCARD_STATE_PRESENT;

            return cc_response_OK;
        } else {
            _rgReaderStates[0].dwCurrentState=SCARD_STATE_EMPTY;
            return cc_response_CardNotFound;
        }
    }
}

- (cc_response) powerOnCard {
	LONG rv;
	rv = SCa02Connect(_hContext, _rgReaderStates[0].szReader,
					  SCARD_SHARE_SHARED, SCARD_PROTOCOL_T0 | SCARD_PROTOCOL_T1,
					  &_hCard, &_dwPref);
	if(rv!=SCARD_S_SUCCESS) {
        // NOTE: For card status, we are always relying on connectCard response for sake of simplicity (separation of concerns)
        err(@"SCardConnect error %x", rv);
        return cc_response_CardError;
	}
	else    {
		if(_hCard)   {
			DWORD dwReaderLen = MAX_READERNAME;
			char* pcReaders = (char *) malloc(sizeof(char) * MAX_READERNAME);
			DWORD dwAtrLen = MAX_ATR_SIZE;
			DWORD dwState,dwProt;
			unsigned char pbAtr[MAX_ATR_SIZE];
			
			rv = SCa02Status(_hCard, pcReaders, &dwReaderLen, &dwState, &dwProt,
							 pbAtr, &dwAtrLen);

            switch (dwProt) {
                case SCARD_PROTOCOL_ANY:
                case SCARD_PROTOCOL_T0:
                    self.active_protocol = PCI_PROTOCOL_0;
                    break;
                case SCARD_PROTOCOL_T1:
                    self.active_protocol = PCI_PROTOCOL_1;
                    break;
                case SCARD_PROTOCOL_RAW:
                    self.active_protocol = PCI_PROTOCOL_Raw;
                    break;
                default:
                    warn(@"Unrecognised protocol");
                    self.active_protocol = PCI_PROTOCOL_Undefined;
            }
            
            _ATR = [NSData dataWithBytes:pbAtr length:dwAtrLen];
            
            logg(@"ATR %@", [NSData dataWithBytes:pbAtr length:dwAtrLen]);
            logg(@"Current reader protocol %d", dwProt - 1);
            return cc_response_OK;
		}
        return cc_response_CardError;
    }
}

- (cc_response) powerOffCard    {
    if(_hCard)  {
        LONG rv = SCa02Disconnect(_hCard, SCARD_RESET_CARD);
        _hCard = 0;
    }
    // rv is ignored, assuming success
    return cc_response_OK;
}

- (APDUResponse *) sendCommand:(NSString *)command {
    APDUResponse *response = [[APDUResponse alloc] init];
	uint8_t pbRecvBuffer[258];
	DWORD cbRecvLength=258;
	LONG rv;
	SCARD_IO_REQUEST pioRecvPci;
 	SCARD_IO_REQUEST pioSendPci;

	switch (self.active_protocol) {
        case PCI_PROTOCOL_0:
            pioSendPci = *SCARD_PCI_T0;
            break;
        case PCI_PROTOCOL_1:
            pioSendPci = *SCARD_PCI_T1;
            break;
        case PCI_PROTOCOL_Raw:
            pioSendPci = *SCARD_PCI_RAW;
            break;
        case PCI_PROTOCOL_Undefined:
        default:
            warn(@"Active protocol not set");
            break;
    }

    int pbSendLength = -1;
    uint8_t *pbSendBuffer = [Common stringToByteArray:command outputLength:&pbSendLength];
    if(pbSendBuffer == NULL)    {
        err(@"Could not create command bytes");
        return nil;
	}
    
	rv=SCa02Transmit(_hCard,&pioSendPci,
					 pbSendBuffer, pbSendLength,
					 &pioRecvPci,pbRecvBuffer,
					 &cbRecvLength);
    response.response_value = rv;
	if(rv==SCARD_S_SUCCESS) {
        logg(@"SCardTransmit successful");
        logg(@"%@",[NSData dataWithBytes:pbRecvBuffer length:cbRecvLength]);
        response.data = [NSData dataWithBytes:pbRecvBuffer length:cbRecvLength-2];
        response.sw1 = pbRecvBuffer[cbRecvLength - 2];
        response.sw2 = pbRecvBuffer[cbRecvLength - 1];        
	} else {
        err(@"SCardTransmit Error:%x", rv);
	}
    return response;
}

#pragma mark - SCard methods (pc/sc)

- (LONG) SCardEstablishContextWithDwScope:(DWORD) dwScope pvReserved1:(LPCVOID) pvReserved1 pvReserved2:(LPCVOID) pvReserved2 phContext: (LPSCARDCONTEXT) phContext  {
    return SCa02EstablishContext(dwScope, pvReserved1, pvReserved2, phContext);
}

- (LONG) SCardIsValidContextWithHContext:(SCARDCONTEXT) hContext    {
    if(!hContext) hContext = _hContext;
    return SCa02IsValidContext(hContext);
}

- (LONG) SCardReleaseContextWithHContext:(SCARDCONTEXT) hContext    {
    if(!hContext) hContext = _hContext;
    return SCa02ReleaseContext(hContext);
}

- (LONG) SCardSetTimeoutWithHContext:(SCARDCONTEXT) hContext dwTimeout:(DWORD) dwTimeout    {
    if(!hContext) hContext = _hContext;    
    return SCa02SetTimeout(hContext, dwTimeout);
}

- (LONG) SCardConnectWithHContext:(SCARDCONTEXT) hContext szReader:(LPCSTR) szReader dwShareMode: (DWORD) dwShareMode dwPreferredProtocols:(DWORD) dwPreferredProtocols phCard:(LPSCARDHANDLE) phCard pdwActiveProtocol:(LPDWORD) pdwActiveProtocol {
    if(!hContext) hContext = _hContext;    
    return SCa02Connect(hContext, szReader, dwShareMode, dwPreferredProtocols, phCard, pdwActiveProtocol);
}

- (LONG) SCardReconnectWithHCard:(SCARDHANDLE) hCard dwShareMode:(DWORD) dwShareMode dwPreferredProtocols:(DWORD) dwPreferredProtocols dwInitialization:(DWORD) dwInitialization pdwActiveProtocol:(LPDWORD) pdwActiveProtocol  {
    if(!hCard)  hCard = _hCard;

    return SCa02Reconnect(hCard, dwShareMode, dwPreferredProtocols, dwInitialization, pdwActiveProtocol);
}

- (LONG) SCardDisconnectWithHCard:(SCARDHANDLE) hCard dwDisposition:(DWORD) dwDisposition   {
    if(!hCard)  hCard = _hCard;
    
    return SCa02Disconnect(hCard, dwDisposition);
}

- (LONG) SCardBeginTransactionWithHCard:(SCARDHANDLE) hCard {
    if(!hCard)  hCard = _hCard;    
    return SCa02BeginTransaction(hCard);
}

- (LONG) SCardEndTransactionWithHCard:(SCARDHANDLE) hCard dwDisposition:(DWORD) dwDisposition   {
    if(!hCard)  hCard = _hCard;
    return SCa02EndTransaction(hCard, dwDisposition);
}

- (LONG) SCardCancelTransactionWithHCard:(SCARDHANDLE) hCard    {
    if(!hCard)  hCard = _hCard;    
    return SCa02CancelTransaction(hCard);
}

- (LONG) SCardStatusWithHCard:(SCARDHANDLE) hCard mszReaderNames:(LPSTR) mszReaderNames pcchReaderLen:(LPDWORD) pcchReaderLen pdwState:(LPDWORD) pdwState pdwProtocol:(LPDWORD) pdwProtocol pbAtr:(LPBYTE) pbAtr pcbAtrLen:(LPDWORD) pcbAtrLen  {
    if(!hCard)  hCard = _hCard;    
    return SCa02Status(hCard, mszReaderNames, pcchReaderLen, pdwState, pdwProtocol, pbAtr, pcbAtrLen);
}

- (LONG) SCardGetStatusChangeWithHContext:(SCARDCONTEXT) hContext dwTimeout:(DWORD) dwTimeout rgReaderStates:(LPSCARD_READERSTATE_A) rgReaderStates cReaders:(DWORD) cReaders   {
    if(!hContext) hContext = _hContext;    
    return SCa02GetStatusChange(hContext, dwTimeout, rgReaderStates, cReaders);
}

- (LONG) SCardControlWithHCard:(SCARDHANDLE) hCard dwControlCode:(DWORD) dwControlCode pbSendBuffer:(const void *)pbSendBuffer cbSendLength:(DWORD) cbSendLength pbRecvBuffer:(void *)pbRecvBuffer cbRecvLength:(DWORD) cbRecvLength lbBytesReturned:(LPDWORD) lpBytesReturned  {
    if(!hCard)  hCard = _hCard;    
    return SCa02Control(hCard, dwControlCode, pbSendBuffer, cbSendLength, pbRecvBuffer, cbRecvLength, lpBytesReturned);
}

- (LONG) SCardTransmitWithHCard:(SCARDHANDLE) hCard pioSendPci:(LPCSCARD_IO_REQUEST) pioSendPci pbSendBuffer:(LPCBYTE) pbSendBuffer cbSendLength:(DWORD) cbSendLength pioRecvPci:(LPSCARD_IO_REQUEST) pioRecvPci pbRecvBuffer:(LPBYTE) pbRecvBuffer pcbRecvLength:(LPDWORD) pcbRecvLength   {
    if(!hCard)  hCard = _hCard;    
    return SCa02Transmit(hCard, pioSendPci, pbSendBuffer, cbSendLength, pioRecvPci, pbRecvBuffer, pcbRecvLength);
}

- (LONG) SCardSecTransmitWithHCard:(SCARDHANDLE) hCard pbSendBuffer:(LPCBYTE) pbSendBuffer cbSendLength:(DWORD) cbSendLength pbRecvBuffer:(LPBYTE) pbRecvBuffer pbRecvLength:(LPDWORD) pcbRecvLength    {
    if(!hCard)  hCard = _hCard;    
    return SCa02SecTransmit(hCard, pbSendBuffer, cbSendLength, pbRecvBuffer, pcbRecvLength);
}

- (LONG) SCardListReaderGroupsWithHContext:(DWORD) hContext mszGroups:(LPCBYTE *) mszGroups pcchGroups:(LPDWORD *)pcchGroups    {    
    warn(@"Unimplemented");
    return SCARD_E_READER_UNSUPPORTED;
}

- (LONG) SCardListReadersWithHContext:(SCARDCONTEXT) hContext mszGroups:(LPCSTR) mszGroups mszReaders:(LPSTR) mszReaders pcchReaders:(LPDWORD) pcchReaders  {
    if(!hContext) hContext = _hContext;    
    return SCa02ListReaders(hContext, mszGroups, mszReaders, pcchReaders);
}

- (LONG) SCardCancel:(SCARDCONTEXT) hContext    {
    if(!hContext) hContext = _hContext;    
    return SCa02Cancel(hContext);
}

@end
