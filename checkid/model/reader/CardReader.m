//
//  CardReader.m
//  checkid
//
//  Created by Ismail Ege AKPINAR on 21/03/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import "CardReader.h"
#import "Debug.h"
#import "Card_eid.h"

@implementation CardReader

@synthesize ATR = _ATR;

#pragma mark - Properties
- (Card *) getCard  {
    if(!_card)  {
        _card = [[Card_eid alloc] initWithReader:self];
    }
    return _card;
}

- (protocol) getActiveProtocol  {
    if(_active_protocol == PCI_PROTOCOL_Undefined)  {
        warn(@"Active protocol undefined, ensure you have powered ON card at least once");
    }
    
    return _active_protocol;
}

- (NSString *) getATR {
    if(_ATR == nil) {
        warn(@"ATR is not determined, ensure you have powered ON card at least once");
    }
    return _ATR;
}

#pragma mark - Abstract methods

- (void) showPairingGUI:(UINavigationController *) nav_controller callback:(void (BOOL success))callback    {
    warn(@"showPairingGUI is not implemented for this read");
}


#pragma mark - SCard methods (pc/sc)

- (LONG) SCardEstablishContextWithDwScope:(DWORD) dwScope pvReserved1:(LPCVOID) pvReserved1 pvReserved2:(LPCVOID) pvReserved2 phContext: (LPSCARDCONTEXT) phContext {
    warn(@"Unimplemented SCard method");
    return SCARD_E_READER_UNSUPPORTED;
}

- (LONG) SCardIsValidContextWithHContext:(SCARDCONTEXT) hContext    {
    warn(@"Unimplemented SCard method");
    return SCARD_E_READER_UNSUPPORTED;
}

- (LONG) SCardReleaseContextWithHContext:(SCARDCONTEXT) hContext    {
    warn(@"Unimplemented SCard method");
    return SCARD_E_READER_UNSUPPORTED;
}


- (LONG) SCardSetTimeoutWithHContext:(SCARDCONTEXT) hContext dwTimeout:(DWORD) dwTimeout    {
    warn(@"Unimplemented SCard method");
    return SCARD_E_READER_UNSUPPORTED;
}

- (LONG) SCardConnectWithHContext:(SCARDCONTEXT) hContext szReader:(LPCSTR) szReader dwShareMode: (DWORD) dwShareMode dwPreferredProtocols:(DWORD) dwPreferredProtocols phCard:(LPSCARDHANDLE) phCard pdwActiveProtocol:(LPDWORD) pdwActiveProtocol {
    warn(@"Unimplemented SCard method");
    return SCARD_E_READER_UNSUPPORTED;
}

- (LONG) SCardReconnectWithHCard:(SCARDHANDLE) hCard dwShareMode:(DWORD) dwShareMode dwPreferredProtocols:(DWORD) dwPreferredProtocols dwInitialization:(DWORD) dwInitialization pdwActiveProtocol:(LPDWORD) pdwActiveProtocol  {
    warn(@"Unimplemented SCard method");
    return SCARD_E_READER_UNSUPPORTED;
}

- (LONG) SCardDisconnectWithHCard:(SCARDHANDLE) hCard dwDisposition:(DWORD) dwDisposition   {
    warn(@"Unimplemented SCard method");
    return SCARD_E_READER_UNSUPPORTED;
}

- (LONG) SCardBeginTransactionWithHCard:(SCARDHANDLE) hCard {
    warn(@"Unimplemented SCard method");
    return SCARD_E_READER_UNSUPPORTED;
}

- (LONG) SCardEndTransactionWithHCard:(SCARDHANDLE) hCard dwDisposition:(DWORD) dwDisposition   {
    warn(@"Unimplemented SCard method");
    return SCARD_E_READER_UNSUPPORTED;
}

- (LONG) SCardCancelTransactionWithHCard:(SCARDHANDLE) hCard    {
    warn(@"Unimplemented SCard method");
    return SCARD_E_READER_UNSUPPORTED;
}

- (LONG) SCardStatusWithHCard:(SCARDHANDLE) hCard mszReaderNames:(LPSTR) mszReaderNames pcchReaderLen:(LPDWORD) pcchReaderLen pdwState:(LPDWORD) pdwState pdwProtocol:(LPDWORD) pdwProtocol pbAtr:(LPBYTE) pbAtr pcbAtrLen:(LPDWORD) pcbAtrLen  {
    warn(@"Unimplemented SCard method");
    return SCARD_E_READER_UNSUPPORTED;
}

- (LONG) SCardGetStatusChangeWithHContext:(SCARDCONTEXT) hContext dwTimeout:(DWORD) dwTimeout rgReaderStates:(LPSCARD_READERSTATE_A) rgReaderStates cReaders:(DWORD) cReaders   {
    warn(@"Unimplemented SCard method");
    return SCARD_E_READER_UNSUPPORTED;
}

- (LONG) SCardControlWithHCard:(SCARDHANDLE) hCard dwControlCode:(DWORD) dwControlCode pbSendBuffer:(const void *)pbSendBuffer cbSendLength:(DWORD) cbSendLength pbRecvBuffer:(void *)pbRecvBuffer cbRecvLength:(DWORD) cbRecvLength lbBytesReturned:(LPDWORD) lpBytesReturned  {
    warn(@"Unimplemented SCard method");
    return SCARD_E_READER_UNSUPPORTED;
}

- (LONG) SCardTransmitWithHCard:(SCARDHANDLE) hCard pioSendPci:(LPCSCARD_IO_REQUEST) pioSendPci pbSendBuffer:(LPCBYTE) pbSendBuffer cbSendLength:(DWORD) cbSendLength pioRecvPci:(LPSCARD_IO_REQUEST) pioRecvPci pbRecvBuffer:(LPBYTE) pbRecvBuffer pcbRecvLength:(LPDWORD) pcbRecvLength   {
    warn(@"Unimplemented SCard method");
    return SCARD_E_READER_UNSUPPORTED;
}

- (LONG) SCardSecTransmitWithHCard:(SCARDHANDLE) hCard pbSendBuffer:(LPCBYTE) pbSendBuffer cbSendLength:(DWORD) cbSendLength pbRecvBuffer:(LPBYTE) pbRecvBuffer pbRecvLength:(LPDWORD) pcbRecvLength    {
    warn(@"Unimplemented SCard method");
    return SCARD_E_READER_UNSUPPORTED;
}

- (LONG) SCardListReaderGroupsWithHContext:(DWORD) hContext mszGroups:(LPCBYTE *) mszGroups pcchGroups:(LPDWORD *)pcchGroups    {
    warn(@"Unimplemented SCard method");
    return SCARD_E_READER_UNSUPPORTED;
}

- (LONG) SCardListReadersWithHContext:(SCARDCONTEXT) hContext mszGroups:(LPCSTR) mszGroups mszReaders:(LPSTR) mszReaders pcchReaders:(LPDWORD) pcchReaders  {
    warn(@"Unimplemented SCard method");
    return SCARD_E_READER_UNSUPPORTED;
}

- (LONG) SCardCancel:(SCARDCONTEXT) hContext    {
    warn(@"Unimplemented SCard method");
    return SCARD_E_READER_UNSUPPORTED;
}

@end
