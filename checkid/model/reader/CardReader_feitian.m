//
//  CardReader_feitian.m
//  checkid
//
//  Created by Ismail Ege AKPINAR on 18/03/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import "CardReader_feitian.h"
#import "Debug.h"
#import "CCCommon.h"
#import "wintypes.h"
#import "winscard_feitian.h"


@implementation CardReader_feitian  {
    SCARDCONTEXT _gContxtHandle;
    SCARDHANDLE _gCardHandle;
}

@synthesize ATR = _ATR;

- (void) config {
    if([NSThread currentThread] != [NSThread mainThread])   {
        [self performSelectorOnMainThread:@selector(config) withObject:nil waitUntilDone:NO];
        return;
    }
    self.type = feitian;
    SCa04EstablishContext(SCARD_SCOPE_SYSTEM,NULL,NULL,&_gContxtHandle);
}

- (void) reset  {
	if(_gContxtHandle)    {
		SCa04ReleaseContext(_gContxtHandle);
		_gContxtHandle=0;
		_gCardHandle=0;
        
	}
}

// NOTE: For feitian, connectCard happens very slow when done in background. Hence, it is delegated to main thread
// TODO: Multi reader - this static variable will be risk for multiple readers
static long return_response;
- (long) connectCard {
    if(![NSThread isMainThread])    {
        [self performSelectorOnMainThread:@selector(connectCard) withObject:nil waitUntilDone:YES];
        return return_response;
    }
   	LONG rv;
    
	int readerCount=0;
	SCARD_READERSTATE rgReaderStates[1];
	
    rv = SCa04GetStatusChange(_gContxtHandle, INFINITE, rgReaderStates, readerCount);
    if(rv != SCARD_S_SUCCESS)   {   // SCARD_E_READER_UNAVAILABLE
        return_response = ID_READER_ABSENT;
        return ID_READER_ABSENT;
    } else {
        DWORD dwState;
        // TODO: This seems to be wrong (instead of context handle, you should pass in card handle which you obtain after powering on the card with SCardConnect!)
        rv = SCa04Status(_gContxtHandle, NULL, NULL, &dwState, NULL, NULL, NULL );
        if (rv != 0) {
            err(@"SCardStatus return ERROR %4x",rv);
            return_response = ID_ERROR;
            return ID_READER_ABSENT;
        }
        
        switch (dwState) {
            case SCARD_ABSENT:
                return_response = ID_CARD_ABSENT;
                return ID_CARD_ABSENT;
            case SCARD_PRESENT:
            case SCARD_SWALLOWED:   // Card not powered
                return_response = ID_CARD_ACCESS_SUCCESSFUL;
                return ID_CARD_ACCESS_SUCCESSFUL;
            default:
                break;
        }
        
        if(rgReaderStates[0].dwEventState & SCARD_STATE_PRESENT)   {
            deb(@"state present");
            return_response = ID_CARD_ACCESS_SUCCESSFUL;
            return ID_CARD_ACCESS_SUCCESSFUL;
        }
        else  {
            return_response = ID_ERROR;
            return ID_ERROR;
        }
    }
}

- (long) powerOnCard {
    if([NSThread currentThread] != [NSThread mainThread])   {
        [self performSelectorOnMainThread:@selector(powerOnCard) withObject:nil waitUntilDone:NO];
        return ID_OK;
    }
    
	LONG iRet = 0;
    DWORD dwActiveProtocol = -1;
    char mszReaders[128] = "";
    DWORD dwReaders = -1;
    
    // TODO: It would increase performance if you removed this listreaders call and do it only once to get the reader name and never do it again
    iRet = SCa04ListReaders(_gContxtHandle, NULL, mszReaders, &dwReaders);
    if(iRet != SCARD_S_SUCCESS) {
        err(@"SCardListReaders error %08x",iRet);
    }
    
    logg(@"mszReaders %s", mszReaders);
    
    iRet = SCa04Connect(_gContxtHandle,mszReaders,SCARD_SHARE_SHARED,SCARD_PROTOCOL_T0 | SCARD_PROTOCOL_T1,&_gCardHandle,&dwActiveProtocol);
	if (iRet != 0) {
        err(@"SCardConnect failed %08x", iRet);
        return ID_ERROR;
	}
	else {
        unsigned char patr[33];
        DWORD len = sizeof(patr);
        iRet = SCa04GetAttrib(_gCardHandle,NULL, patr, &len);
        if(iRet != SCARD_S_SUCCESS) {
            err(@"SCardGetAttrib error %08x",iRet);
        }
        
		NSMutableData *tmpData = [NSMutableData data];
        [tmpData appendBytes:patr length:len];
        
        NSString* dataString= [NSString stringWithFormat:@"%@",tmpData];
        NSRange begin = [dataString rangeOfString:@"<"];
        NSRange end = [dataString rangeOfString:@">"];
        NSRange range = NSMakeRange(begin.location + begin.length, end.location- begin.location - 1);
        dataString = [dataString substringWithRange:range];
        
        _ATR = tmpData;
        _ATR = [CCCommon dataToString:tmpData];
        logg(@"ATR %@", _ATR);

        DWORD pcchReaderLen;
        DWORD pdwState;
        DWORD pdwProtocol;
        len = sizeof(patr);
        pcchReaderLen = sizeof(mszReaders);
        
        iRet =  SCa04Status(_gCardHandle,mszReaders,&pcchReaderLen,&pdwState,&pdwProtocol,patr,&len);

        switch (pdwProtocol) {
            case SCARD_PROTOCOL_ANY:
            case SCARD_PROTOCOL_T0:
                self.active_protocol = PCI_PROTOCOL_0;
                break;
            case SCARD_PROTOCOL_T1:
                self.active_protocol = PCI_PROTOCOL_1;
                break;
            case SCARD_PROTOCOL_RAW:
                self.active_protocol = PCI_PROTOCOL_Raw;
                break;
            default:
                warn(@"Unrecognised protocol");
                self.active_protocol = PCI_PROTOCOL_Undefined;
        }
        
        if(iRet != SCARD_S_SUCCESS) {
            err(@"SCardStatus error %08x",iRet);
            return ID_ERROR;
        }
        return ID_OK;
    }
}

- (long)powerOffCard {
	LONG iRet = 0;
    iRet = SCa04Disconnect(_gCardHandle,SCARD_UNPOWER_CARD);
	if (iRet != 0) {
        err(@"PowerOff failed - ret %d.\n", iRet);
        return ID_ERROR;
	}
	else    {
        logg(@"PowerOff successful");
        return ID_OK;
	}
    
}

- (APDUResponse *) sendCommand:(NSString *)command {
    APDUResponse *response = [[APDUResponse alloc] init];
	LONG iRet = 0;
    int capdulen;
	unsigned char *capdu = [CCCommon stringToByteArray:command outputLength:&capdulen];
	unsigned char resp[512];
	unsigned int resplen = sizeof(resp) ;
    

    resplen = sizeof(resp);
    SCARD_IO_REQUEST pioSendPci;
	switch (self.active_protocol) {
        case PCI_PROTOCOL_0:
            pioSendPci = *SCARD_PCI_T0;
            break;
        case PCI_PROTOCOL_1:
            pioSendPci = *SCARD_PCI_T1;
            break;
        case PCI_PROTOCOL_Raw:
            pioSendPci = *SCARD_PCI_RAW;
            break;
        case PCI_PROTOCOL_Undefined:
        default:
            warn(@"Active protocol not set");
            break;
    }
    
    iRet=SCa04Transmit(_gCardHandle,
                       &pioSendPci,
                       (unsigned char*)capdu,
                       capdulen,
                       NULL,
                       resp,
                       &resplen);
    response.response_value = iRet;
	if (iRet != 0) {
		warn(@"SCardTransmit failed - ret %08X.", iRet);
	}
	else {
        response.data = [NSData dataWithBytes:resp length:resplen-2];
        response.sw1 = resp[resplen - 2];
        response.sw2 = resp[resplen - 1];
	}
    return response;
}

#pragma mark - SCard methods (pc/sc)

- (LONG) SCardEstablishContextWithDwScope:(DWORD) dwScope pvReserved1:(LPCVOID) pvReserved1 pvReserved2:(LPCVOID) pvReserved2 phContext: (LPSCARDCONTEXT) phContext  {
    return SCa04EstablishContext(dwScope, pvReserved1, pvReserved2, phContext);
}

- (LONG) SCardIsValidContextWithHContext:(SCARDCONTEXT) hContext    {
    if(!hContext) hContext = _gContxtHandle;
    return SCa04IsValidContext(hContext);
}

- (LONG) SCardReleaseContextWithHContext:(SCARDCONTEXT) hContext    {
    if(!hContext) hContext = _gContxtHandle;
    return SCa04ReleaseContext(hContext);
}

- (LONG) SCardSetTimeoutWithHContext:(SCARDCONTEXT) hContext dwTimeout:(DWORD) dwTimeout    {
    if(!hContext) hContext = _gContxtHandle;
    return SCa04SetTimeout(hContext, dwTimeout);
}

- (LONG) SCardConnectWithHContext:(SCARDCONTEXT) hContext szReader:(LPCSTR) szReader dwShareMode: (DWORD) dwShareMode dwPreferredProtocols:(DWORD) dwPreferredProtocols phCard:(LPSCARDHANDLE) phCard pdwActiveProtocol:(LPDWORD) pdwActiveProtocol {
    if(!hContext) hContext = _gContxtHandle;
    return SCa04Connect(hContext, szReader, dwShareMode, dwPreferredProtocols, phCard, pdwActiveProtocol);
}

- (LONG) SCardReconnectWithHCard:(SCARDHANDLE) hCard dwShareMode:(DWORD) dwShareMode dwPreferredProtocols:(DWORD) dwPreferredProtocols dwInitialization:(DWORD) dwInitialization pdwActiveProtocol:(LPDWORD) pdwActiveProtocol  {
    if(!hCard)  hCard = _gCardHandle;
    
    return SCa04Reconnect(hCard, dwShareMode, dwPreferredProtocols, dwInitialization, pdwActiveProtocol);
}

- (LONG) SCardDisconnectWithHCard:(SCARDHANDLE) hCard dwDisposition:(DWORD) dwDisposition   {
    if(!hCard)  hCard = _gCardHandle;
    
    return SCa04Disconnect(hCard, dwDisposition);
}

- (LONG) SCardBeginTransactionWithHCard:(SCARDHANDLE) hCard {
    if(!hCard)  hCard = _gCardHandle;
    return SCa04BeginTransaction(hCard);
}

- (LONG) SCardEndTransactionWithHCard:(SCARDHANDLE) hCard dwDisposition:(DWORD) dwDisposition   {
    if(!hCard)  hCard = _gCardHandle;
    return SCa04EndTransaction(hCard, dwDisposition);
}

- (LONG) SCardCancelTransactionWithHCard:(SCARDHANDLE) hCard    {
    warn(@"Unimplemented");
    return SCARD_E_READER_UNSUPPORTED;
}

- (LONG) SCardStatusWithHCard:(SCARDHANDLE) hCard mszReaderNames:(LPSTR) mszReaderNames pcchReaderLen:(LPDWORD) pcchReaderLen pdwState:(LPDWORD) pdwState pdwProtocol:(LPDWORD) pdwProtocol pbAtr:(LPBYTE) pbAtr pcbAtrLen:(LPDWORD) pcbAtrLen  {
    if(!hCard)  hCard = _gCardHandle;
    return SCa04Status(hCard, mszReaderNames, pcchReaderLen, pdwState, pdwProtocol, pbAtr, pcbAtrLen);
}

- (LONG) SCardGetStatusChangeWithHContext:(SCARDCONTEXT) hContext dwTimeout:(DWORD) dwTimeout rgReaderStates:(LPSCARD_READERSTATE_A) rgReaderStates cReaders:(DWORD) cReaders   {
    if(!hContext) hContext = _gContxtHandle;
    return SCa04GetStatusChange(hContext, dwTimeout, rgReaderStates, cReaders);
}

- (LONG) SCardControlWithHCard:(SCARDHANDLE) hCard dwControlCode:(DWORD) dwControlCode pbSendBuffer:(const void *)pbSendBuffer cbSendLength:(DWORD) cbSendLength pbRecvBuffer:(void *)pbRecvBuffer cbRecvLength:(DWORD) cbRecvLength lbBytesReturned:(LPDWORD) lpBytesReturned  {
    if(!hCard)  hCard = _gCardHandle;
    return SCa04Control(hCard, dwControlCode, pbSendBuffer, cbSendLength, pbRecvBuffer, cbRecvLength, lpBytesReturned);
}

- (LONG) SCardTransmitWithHCard:(SCARDHANDLE) hCard pioSendPci:(LPCSCARD_IO_REQUEST) pioSendPci pbSendBuffer:(LPCBYTE) pbSendBuffer cbSendLength:(DWORD) cbSendLength pioRecvPci:(LPSCARD_IO_REQUEST) pioRecvPci pbRecvBuffer:(LPBYTE) pbRecvBuffer pcbRecvLength:(LPDWORD) pcbRecvLength   {
    if(!hCard)  hCard = _gCardHandle;
    return SCa04Transmit(hCard, pioSendPci, pbSendBuffer, cbSendLength, pioRecvPci, pbRecvBuffer, pcbRecvLength);
}

- (LONG) SCardSecTransmitWithHCard:(SCARDHANDLE) hCard pbSendBuffer:(LPCBYTE) pbSendBuffer cbSendLength:(DWORD) cbSendLength pbRecvBuffer:(LPBYTE) pbRecvBuffer pbRecvLength:(LPDWORD) pcbRecvLength    {
    warn(@"Unimplemented");
    return SCARD_E_READER_UNSUPPORTED;
}

- (LONG) SCardListReaderGroupsWithHContext:(DWORD) hContext mszGroups:(LPCBYTE *) mszGroups pcchGroups:(LPDWORD *)pcchGroups    {
    warn(@"Unimplemented");
    return SCARD_E_READER_UNSUPPORTED;
}

- (LONG) SCardListReadersWithHContext:(SCARDCONTEXT) hContext mszGroups:(LPCSTR) mszGroups mszReaders:(LPSTR) mszReaders pcchReaders:(LPDWORD) pcchReaders  {
    if(!hContext) hContext = _gContxtHandle;
    return SCa04ListReaders(hContext, mszGroups, mszReaders, pcchReaders);
}

- (LONG) SCardCancel:(SCARDCONTEXT) hContext    {
    if(!hContext) hContext = _gContxtHandle;
    return SCa04Cancel(hContext);
}

@end
