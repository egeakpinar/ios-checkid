//
//  Card.h
//  checkid
//
//  Created by Ismail Ege AKPINAR on 18/03/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CardReader.h"

#define CA_PIN_VALID 99999

@interface Card : NSObject

- (id) initWithReader:(CardReader *)reader;

- (NSString *) getATR;
- (int) verifyPIN:(NSString *)PIN;  // If PIN valid, returns CA_PIN_VALID, else returns number of attempts left. Returns -1 if command fails
@end
