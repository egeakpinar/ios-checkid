//
//  Card_eid.m
//  checkid
//
//  Created by Ismail Ege AKPINAR on 18/03/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import "Card_eid.h"
#import "Debug.h"

@interface Card_eid()
    @property(nonatomic, retain) CardReader *reader;
@end

@implementation Card_eid

#pragma mark - Init method
- (id) initWithReader:(CardReader *)reader    {
    if(self = [super init])  {
        _reader = reader;
    }
    return self;
}

#pragma mark - Card protocol methods
- (int) verifyPIN:(NSString *)PIN   {
    if(!self.reader)    {
        err(@"CardReader parent not set on Card_eid object");
        return ID_ERROR;
    }
    
    APDUResponse *response = [self.reader sendCommand:[NSString stringWithFormat:@"002000010824%@FFFFFFFFFF", PIN]];
    [response log];
    if (![response isOK]) {
        // Denotes whether command is sent successfuly or not, doesn't have anything to do with PIN response meaning
        return -1;
    }
    // TODO: Return PIN valid or number of attempts left here
    return ID_OK;
    //    return [response isOk] ? 3 : response.sw1Ptr == 0x63 ? response.sw2Ptr %16 : 0;
}

@end
